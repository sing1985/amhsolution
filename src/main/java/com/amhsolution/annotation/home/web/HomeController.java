package com.amhsolution.annotation.home.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amhsolution.annotation.home.service.HomeService;
import com.amhsolution.common.component.CommonInterface;
import com.amhsolution.common.page.PageAccess;

import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;


@Controller
public class HomeController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	
	@Resource(name="HomeService")
	private HomeService HomeService;
	/** 페이지 처리 출 */ 
	@Autowired private PageAccess PageAccess;
	/** 공통 컴포넌트 */
	@Autowired CommonInterface Component;  
	
	@RequestMapping(value = {"/home"})
	public String home(Model model,
			PaginationInfo pageInfo,
			@RequestParam Map<String,Object> map
			) throws Exception{
		
		if( pageInfo.getCurrentPageNo() == 0 ){
			pageInfo.setCurrentPageNo(1);
		}
		pageInfo = PageAccess.getPagInfo(pageInfo.getCurrentPageNo(), "CS.NOTICE_LIST_PAGING_COUNT", map);
		map.put("firstIndex", pageInfo.getFirstRecordIndex());
		map.put("recordCountPerPage", pageInfo.getRecordCountPerPage());
		List resultList = Component.getList("CS.NOTICE_LIST_PAGING", map);
		
		pageInfo = PageAccess.getPagInfo(pageInfo.getCurrentPageNo(), "CS.ASK_LIST_PAGING_COUNT", map);
		List resultList2 = Component.getList("CS.ASK_LIST_PAGING", map);
		
		
		
		model.addAttribute("resultList", resultList);
		model.addAttribute("resultList2", resultList2);
		
		model.addAttribute("PAGE_NAME_BIG","HOME");
		model.addAttribute("PAGE_NAME_SMALL","회사소개");
		return "/home.home";
	} 
	
	
	/**
	 * 회사소개 메인페이지
	*/
	@RequestMapping(value = "/overview")
	public String overview(@RequestParam Map<String,Object> map, Model model) throws Exception{
		model.addAttribute("map", map);
		model.addAttribute("PAGE_NAME_BIG","OVERVIEW");
		model.addAttribute("PAGE_NAME_SMALL","회사소개");
		return "/main/overview/overview.tiles";
	} 
	
	/**
	 * 회사소개 서브페이지 1/2/3/4
	*/
	@RequestMapping(value = "/overview1")
	public String overview1(Model model) throws Exception{
		return "/main/overview/overview1";
	} 
	@RequestMapping(value = "/overview2")
	public String overview2(Model model) throws Exception{
		return "/main/overview/overview2";
	} 
	@RequestMapping(value = "/overview3")
	public String overview3(Model model) throws Exception{
		return "/main/overview/overview3";
	} 
	@RequestMapping(value = "/overview4")
	public String overview4(Model model) throws Exception{
		return "/main/overview/overview4";
	}
	
	
	
	
	
	/**
	 * 제품소개 메인페이지
	*/
	@RequestMapping(value = "/product")
	public String product(@RequestParam Map<String,Object> map, Model model) throws Exception{
		model.addAttribute("map", map);
		model.addAttribute("PAGE_NAME_BIG","PRODUCT");
		model.addAttribute("PAGE_NAME_SMALL","제품소개");
		return "/main/product/product.tiles";
	} 
	
	/**
	 * 제품소개 서브페이지 1/2/3/4
	*/
	@RequestMapping(value = "/product1")
	public String product1(Model model) throws Exception{
		return "/main/product/product1";
	} 
	@RequestMapping(value = "/product2")
	public String product2(Model model) throws Exception{
		return "/main/product/product2";
	} 
	@RequestMapping(value = "/product3")
	public String product3(Model model) throws Exception{
		return "/main/product/product3";
	} 
	@RequestMapping(value = "/product4")
	public String product4(Model model) throws Exception{
		return "/main/product/product4";
	}
	
	
	
	
	
	/**
	 * 고객센터 메인페이지
	*/
	@RequestMapping(value = "/cs")
	public String cs(
			@RequestParam Map<String,Object> map,
			Model model) throws Exception{
		
		model.addAttribute("map", map);
		model.addAttribute("PAGE_NAME_BIG","CS");
		model.addAttribute("PAGE_NAME_SMALL","고객센터");
		return "/main/cs/cs.tiles";
	} 
	
	/**
	 * 고객센터 서브페이지 1/2/3/4
	*/
	@RequestMapping(value = "/cs1")
	public String cs1(Model model,
			PaginationInfo pageInfo,
			@RequestParam Map<String,Object> map
			) throws Exception{
		
		if( pageInfo.getCurrentPageNo() == 0 ){
			pageInfo.setCurrentPageNo(1);
		}
		pageInfo = PageAccess.getPagInfo(pageInfo.getCurrentPageNo(), "CS.NOTICE_LIST_PAGING_COUNT", map);
//		System.err.println("#001 : " + pageInfo.getFirstRecordIndex() +", " + pageInfo.getLastRecordIndex() + ", " + pageInfo.getRecordCountPerPage() + ", " + pageInfo.getTotalRecordCount());
		map.put("firstIndex", pageInfo.getFirstRecordIndex());
		map.put("recordCountPerPage", pageInfo.getRecordCountPerPage());
		//		News.setFirstIndex(pageInfo.getFirstRecordIndex());
//		News.setLastIndex(pageInfo.getLastRecordIndex());
//		News.setRecordCountPerPage(pageInfo.getRecordCountPerPage());
		List resultList = Component.getList("CS.NOTICE_LIST_PAGING", map);
		
		model.addAttribute("resultList", resultList);
		model.addAttribute("paginationInfo", pageInfo);
		
		return "/main/cs/cs1";
	}
	
	@RequestMapping(value = "/cs1/detail")
	public String cs1_detail(Model model,
			@RequestParam Map<String,Object> map
			) throws Exception{
		HashMap<String,String> resultDetail = Component.getData("CS.NOTICE_DETAIL", map);
		
		model.addAttribute("resultDetail", resultDetail);
		model.addAttribute("map", map); // currentPageNo
		
		return "/main/cs/cs1_detail";
	}
	
	
	@RequestMapping(value = "/cs1/regist/view")
	public String cs1_registView(Model model,
			@RequestParam Map<String,Object> map
			) throws Exception{
		model.addAttribute("map", map);
		return "/main/cs/cs1_regist";
	}
	
	@RequestMapping(value = "/cs1/regist/action")
	public String cs1_registAction(Model model,
			@RequestParam Map<String,Object> map
			) throws Exception{
		
		Component.createData("CS.NOTICE_REGIST_ACTION", map);
		
		return "redirect:/cs1.do";
	}
	
	
	@RequestMapping(value = "/cs1/modify/view")
	public String cs1_modifyView(Model model,
			@RequestParam Map<String,Object> map
			) throws Exception{
		model.addAttribute("map", map);
		return "/main/cs/cs1_modify";
	}
	
	
	@RequestMapping(value = "/cs1/modify/action")
	public String cs1_modifyAction(Model model,
			@RequestParam Map<String,Object> map
			) throws Exception{
		
		String B_KEYNO = (String) map.get("B_KEYNO");
		String currentPageNo = (String) map.get("currentPageNo");
		
		Component.updateData("CS.NOTICE_UPDATE_ACTION", map);

		return "redirect:/cs1/detail.do?B_KEYNO="+B_KEYNO+"&currentPageNo="+currentPageNo;
//		return "redirect:/cs2.do";
	}
	
	
	@RequestMapping(value = "/cs2")
	public String cs2(Model model,
			PaginationInfo pageInfo,
			@RequestParam Map<String,Object> map
			) throws Exception{
		
		if( pageInfo.getCurrentPageNo() == 0 ){
			pageInfo.setCurrentPageNo(1);
		}
		pageInfo = PageAccess.getPagInfo(pageInfo.getCurrentPageNo(), "CS.ASK_LIST_PAGING_COUNT", map);
//		System.err.println("#001 : " + pageInfo.getFirstRecordIndex() +", " + pageInfo.getLastRecordIndex() + ", " + pageInfo.getRecordCountPerPage() + ", " + pageInfo.getTotalRecordCount());
		map.put("firstIndex", pageInfo.getFirstRecordIndex());
		map.put("recordCountPerPage", pageInfo.getRecordCountPerPage());
		//		News.setFirstIndex(pageInfo.getFirstRecordIndex());
//		News.setLastIndex(pageInfo.getLastRecordIndex());
//		News.setRecordCountPerPage(pageInfo.getRecordCountPerPage());
		List resultList = Component.getList("CS.ASK_LIST_PAGING", map);
		
		model.addAttribute("resultList", resultList);
		model.addAttribute("paginationInfo", pageInfo);
		
		return "/main/cs/cs2";
	}
	
	@RequestMapping(value = "/cs2/detail")
	public String cs2_detail(Model model,
			@RequestParam Map<String,Object> map
			) throws Exception{
		
		System.err.println("#001 /cs2/detail - B_ASK_KEYNO : " + map.get("B_KEYNO"));
		HashMap<String,String> resultDetail = Component.getData("CS.ASK_DETAIL", map);
		
		model.addAttribute("resultDetail", resultDetail);
		model.addAttribute("map", map);
		
		return "/main/cs/cs2_detail";
	}
	
	@RequestMapping(value = "/cs2/regist/view")
	public String cs2_registView(Model model,
			@RequestParam Map<String,Object> map,
			HttpServletRequest request
			) throws Exception{
		model.addAttribute("LoginInfo", request.getSession().getAttribute("LoginInfo"));
		model.addAttribute("map", map);
		return "/main/cs/cs2_regist";
	}
	
	@RequestMapping(value = "/cs2/regist/action")
	public String cs2_registAction(Model model,
			@RequestParam Map<String,Object> map
			) throws Exception{
		
		Component.createData("CS.ASK_REGIST_ACTION", map);
		return "redirect:/cs2.do"; 
	}
	
	
	@RequestMapping(value = "/cs2/modify/action")
	public String cs2_modifyAction(Model model,
			@RequestParam Map<String,Object> map
			) throws Exception{
		
		Component.updateData("CS.ASK_UPDATE_ACTION", map);
		
		String B_KEYNO = (String) map.get("B_KEYNO");
		String B_PWD = (String) map.get("B_PWD");
		String currentPageNo = (String) map.get("currentPageNo");
		
		return "redirect:/cs2/detail.do?B_KEYNO="+B_KEYNO+"&currentPageNo="+currentPageNo+"&B_PWD="+B_PWD;
//		return "redirect:/cs2.do";
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/cs2/detail/chk/password")
	public String cs2_detail_password(Model model,
			@RequestParam Map<String,Object> map
			) throws Exception{
		
		System.err.println("#001 - B_NT_KEYNO : " + map.get("B_PWD"));
		HashMap<String,String> resultDetail = Component.getData("CS.ASK_PASSWORD", map);
		
		model.addAttribute("resultDetail", resultDetail);
		model.addAttribute("map", map);
		
		if( resultDetail == null ){
			System.err.println("#002 : result is NULL");
			return "false";
		}else{
			return resultDetail.get("B_ASK_PASSWORD");
		}
		
	}
	
	
	@RequestMapping(value = "/cs3")
	public String cs3(Model model) throws Exception{
		return "/main/cs/cs3";
	} 
	@RequestMapping(value = "/cs4")
	public String cs4(Model model) throws Exception{
		return "/main/cs/cs4";
	}
	
	
	
	@RequestMapping(value = "/login/page")
	public String login_page(Model model,
			@RequestParam Map<String,Object> map
			) throws Exception{
		model.addAttribute("map", map);
		return "/login/login.tiles";
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/login/check")
	public String login_pwd_check(Model model,
			@RequestParam Map<String,Object> map,
			HttpServletRequest request
			) throws Exception{
		
		System.err.println("#001 - UI_ID : " + map.get("ID"));
		HashMap<String,String> resultDetail = Component.getData("CS.LOGIN_PASSWORD_CHK", map);
		
		model.addAttribute("resultDetail", resultDetail);
		model.addAttribute("map", map);
		
		if( resultDetail == null ){
			System.err.println("#002 : result is NULL");
			return "false";
		}else{
			request.getSession().setAttribute("LoginInfo", resultDetail );
			return resultDetail.get("UI_PASSWORD");
		}
	}
	
	
	@RequestMapping(value = "/login/action")
	public String login_action(Model model,
			@RequestParam Map<String,Object> map,
			HttpServletRequest request
			) throws Exception{
//		request.getSession().setAttribute("LoginID", model.addAttribute("ID") );
//		request.getSession().setAttribute("LoginID", model.addAttribute("ID") );
		
		model.addAttribute("map", map);
		
		return "redirect:/home.do";
	}
	
	
	
	@RequestMapping(value = "/logout")
	public String login_logout(Model model,
			@RequestParam Map<String,Object> map,
			HttpServletRequest request
			) throws Exception{

		request.getSession().removeAttribute("LoginInfo");
		request.getSession().removeAttribute("LoginID");
		
		return "redirect:/home.do";
	}
	
	
	
	@RequestMapping(value = "/join")
	public String joinUser_view(Model model,
			@RequestParam Map<String,Object> map,
			HttpServletRequest request
			) throws Exception{
		
		return "/login/join.tiles";
	}

	
	@RequestMapping(value = "/join/action")
	public String joinUser_action(Model model,
			@RequestParam Map<String,Object> map,
			HttpServletRequest request
			) throws Exception{
		
		Component.createData("CS.USER_REGIST_ACTION", map);
		
		return "redirect:/login/page.do?ID="+map.get("UI_ID");
	}
	
	
}
