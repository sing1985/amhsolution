package com.amhsolution.annotation.home.service.impl;

import org.springframework.stereotype.Repository;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

@Repository("HomeManageDAO")
public class HomeManageDAO extends EgovAbstractDAO{

	public int SampleSearch()throws Exception{
		return (Integer) selectByPk("HomeManageDAO.SampleSearch", "");
	}
}
