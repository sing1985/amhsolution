package com.amhsolution.annotation.home.service.impl;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.amhsolution.annotation.home.service.HomeService;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
@Service("HomeService")
public class HomeServiceImpl extends AbstractServiceImpl implements HomeService {

	@Resource(name="HomeManageDAO")
    private HomeManageDAO HomeManageDAO;
	
	public int SampleSearch()throws Exception{
		return HomeManageDAO.SampleSearch();
	}


} 
