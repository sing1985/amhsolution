package com.amhsolution.common.filter;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
 
public class RequestWrapper2 extends HttpServletRequestWrapper {

private static Pattern[] patterns = new Pattern[] {

};

/**
 * @param servletRequest
 * @comment XSS 필터 패턴이 없는 빈 메소드 - 기존 필터처리 경로와 호환성 유지를 위해 생성
 */
public RequestWrapper2(HttpServletRequest servletRequest) {
    super(servletRequest);
}

@Override
public String[] getParameterValues(String parameter) {
    String[] values = super.getParameterValues(parameter);
    if (values == null) {
        return null;
    }
    int count = values.length;
    String[] encodedValues = new String[count];
    for (int i = 0; i < count; i++) {
        encodedValues[i] = stripXSS(values[i]);
    }
    return encodedValues;
}

@Override
public String getParameter(String parameter) {
    String value = super.getParameter(parameter);
    return stripXSS(value);
}

@Override
public String getHeader(String name) {
    String value = super.getHeader(name);
    return stripXSS(value);
}

private String stripXSS(String value) {
   if (value != null) {

   // ESAPI library 이용하여 XSS 필터를 적용하려면 아래 코드의 커맨트
   // 를 제거하고 사용한다. 강력추천!!
   // value = ESAPI.encoder().canonicalize(value);

   // null 문자를 제거한다.
//   value = value.replaceAll("\0", "");

   // 패턴을 포함하는 입력에 대해 <, > 을 인코딩한다.
//   for (Pattern scriptPattern : patterns) {
//      if ( scriptPattern.matcher(value).find() ) {
//         System.out.println("match.....");
//         value=value.replaceAll("<", "&lt;").replaceAll(">", "&gt;");   }
//      } 
    }
    return value;
  }
}