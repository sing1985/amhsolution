package com.amhsolution.common.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class CrossScriptingFilter implements Filter {
	public void destroy() {
	}

	private String[] excludePatternArr;

	public void init(FilterConfig filterConfig) throws ServletException {
		String excludePatterns = filterConfig.getInitParameter("excludePatterns");
		this.excludePatternArr = excludePatterns.split(",");
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		String url = ((HttpServletRequest) request).getRequestURI().toString();
		if (matchExcludePatterns(url)) {
			chain.doFilter(new RequestWrapper((HttpServletRequest) request), response);
		}else{
			chain.doFilter(new RequestWrapper2((HttpServletRequest) request), response); // XSS조건 없는 빈 필터
		}
	}

	public boolean matchExcludePatterns(String url) {
//		System.err.println(this.excludePatterns + "VS #+70 : "+ url + ", " + url.indexOf(this.excludePatterns) );
		if (url == null) {
			return false;
		}
		for( String excludePattern : excludePatternArr){
			if ( url.indexOf(excludePattern) > -1 ) { // 예외처리
//			System.err.println("#예외처리 성공");
				return false;
			}
		}
		return true;
	}
}
