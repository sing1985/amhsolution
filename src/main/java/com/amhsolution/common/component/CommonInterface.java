package com.amhsolution.common.component;

import java.util.List;

import org.springframework.dao.DataAccessException;


/**
 * @CommonInterface
 * 공통 인터 페이스를 관리한다.
 * @author 신희원
 * @version 1.0
 * @since 2014-11-14
 */
public interface CommonInterface {
	
	/**
	 * 데이터 개수 조회
	 * @param query
	 * @param model
	 * @return
	 * @throws DataAccessException
	 */
    public Integer getCount(String query, Object model) throws DataAccessException;
	
    /**
	 * 데이터 개수 조회
	 * @param query
	 * @return
	 * @throws DataAccessException
	 */
    public Integer getCount(String query) throws DataAccessException;
    
    /**
     * 목록 조회
     * @param <E>
     * @param query
     * @param model
     * @return
     * @throws DataAccessException
   */
    public <E> List<E> getList(String query, Object model) throws DataAccessException;
   
    /**
     * 파라미터 값 없는 목록 조회
     * @param <E>
     * @param query
     * @param model
     * @return
     * @throws DataAccessException
     */
    public <E> List<E> getListNoParam(String query) throws DataAccessException;
    
    /**
     * 페이지 구분 목록 조회
     * @param query
     * @param model
     * @return
     * @throws DataAccessException
     */
    public <E> List<E> getPageList(String query, Object model) throws DataAccessException;

    /**
     * 데이터 조회 (Object)
     * @param query
     * @param model
     * @return
     * @throws DataAccessException
     */
    public <E> E getData(String query, Object model) throws DataAccessException;
    
    /**
     * 데이터 삽입 
     * @param query
     * @param model
     * @return Obj
     * @throws DataAccessException
     */
    public void createData(String query, Object model) throws DataAccessException;   
 
    /**
     * 데이터 삽입 후 selectKey 값 반환
     * @param query
     * @param model
     * @return Integer
     * @throws DataAccessException
     */
    public Integer createData_key(String query, Object model) throws DataAccessException;
   
    /**
     * 데이터 수정 (Yes Obj)
     * @param query
     * @param Object
     * @return
     * @throws DataAccessException
     */
    public void updateData(String query, Object model) throws DataAccessException;

    /**
     * 데이터 삭제
     * @param query
     * @param model
     * @throws DataAccessException
     */
    public void deleteData(String query, Object model) throws DataAccessException;
    
    /**
     * 테이블 고유 키 생성
     * @param query
     * @param model
     * @throws DataAccessException
     */
    public String getTableKey(String TableName) throws DataAccessException;
    
    /**
     * 테이블 코드로 테이블 명칭 얻기
     * @param TableCode
     * @return
     */
    public String getTableName(String TableCode);
    
    /**
	 * 검색조건으로 받아온 값을 각 테이블의 컬럼명으로 변환 
	 * @param name 
	 * */
	public String[] setConditions(String searchCondition, String name);
	
	/**
	 * 받아온 keyno값을 테이블 규칙에 맞게 변경
	 * ex) 123 -> SC_0000000123
	 */
	public String getKeyno(String keyno,String tableName);
	
} 
