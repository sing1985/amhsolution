package com.amhsolution.common.component.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.amhsolution.common.component.CommonInterface;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;


/**
 * @CommonDataAccessObject
 * 공통 인터 페이스의 처리 내용을 관리한다.
 * @author 신희원
 * @version 1.0
 * @since 2014-11-14
 */
@Repository
public class CommonDataAccessObject extends EgovAbstractDAO implements CommonInterface {
 
	
	public Integer getCount(String query, Object search) throws DataAccessException {
		return (Integer) selectByPk(query, search);
	}
	
	
    public Integer getCount(String query) throws DataAccessException {
		return (Integer) selectByPk(query, null);
	}
    
	@SuppressWarnings("unchecked")
	
	public <E> List getList(String query, Object search) throws DataAccessException {
		return (List<E>) list(query, search);
	}
	
    public <E> List<E> getListNoParam(String query) throws DataAccessException{
		return (List<E>) list(query, null);
	}
	
	
	public <E> List<E> getPageList(String query, Object search) throws DataAccessException {
		return getList(query, search);
	}

	@SuppressWarnings("unchecked")
	
	public <E> E getData(String query, Object search) throws DataAccessException { 
		return (E) selectByPk(query, search);
	}

	
	public void createData(String query, Object model) throws DataAccessException {
		insert(query, model);
	}

	
	public Integer createData_key(String query, Object model) throws DataAccessException {
    int key = (Integer) insert(query, model);
    return key;
  }
	
	
	public void updateData(String query, Object model) throws DataAccessException {
		update(query, model);
	}

	
	public void deleteData(String query, Object search) throws DataAccessException {
		delete(query, search);
	}
	

	/**
	 * 테이블 키 정보 조회 후 새키 번호 발급
	 */
	
    public synchronized String getTableKey(String TableCode) throws DataAccessException{
		
		//테이블 명 필터 및 갱신 처리
		String tableName = getTableName(TableCode);
		
		//테이블 코드가 없는 경우
		if(tableName == null){
			return null; // 잘못된 파라미터를 받을 경우 null로 Return
		}
		
		//테이블 현재 카운팅 숫자 조회
		int getKeyNum = getData("Common.getTableKey", tableName);
		getKeyNum = getKeyNum + 1;
		
		//카운터 갱신 처리
		updateData("Common.CountTableKey", tableName);
		String CodeColumn = Integer.toString(getKeyNum);
		
		//코드 발급
		while(CodeColumn.length() < 10){
			CodeColumn = "0" +  CodeColumn;
		}
		return TableCode + "_" + CodeColumn;
	}
	
	/**
	 * 테이블 코드 번호를 테이블 정보로 리턴
	 */
	
	public String getTableName(String TableCode){
		String TableName = "";
		switch (TableCode) {
			 case "MN" 		 : TableName = "T_MENU_MANAGER"; break;						// 메뉴 관리 테이블
			 case "BM" 		 : TableName = "T_BUSINESS_MAIN_MANAGER"; break;			// 메뉴 관리 테이블
			 case "BS" 		 : TableName = "T_BUSINESS_SUB_MANAGER"; break;				// 메뉴 관리 테이블
			 case "BB" 		 : TableName = "T_BUSINESS_BOARD_MANAGER"; break;			// 메뉴 관리 테이블
			 case "FM" 		 : TableName = "T_COMMON_FILE_MAIN"; break;					// 공통관리 - 파일관리 - 그룹상위정보관리
			 case "FS"	 	 : TableName = "T_COMMON_FILE_SUB"; break;					// 공통관리 - 파일관리 - 상세파일정보관리
			 case "CM" 	 	 : TableName = "T_CONTENTS_MAIN_SLIDE_MANAGER"; break;		// 프로그램 - 기본 관리 - 메인슬라이드 관리
			 case "MC"  	 : TableName = "T_COMMON_CODE_MAIN"; break; 				// 상위코드 관리 테이블
		     case "SC"  	 : TableName = "T_COMMON_CODE_SUB"; break;					// 하위코드 관리 테이블
		     case "GO"  	 : TableName = "T_GOVINFO_MANAGER"; break;					// 정부3.0정보공개 - 관계법령서 및 서식다운로드
		     case "IM"  	 : TableName = "T_INTRO_MANAGER"; break;					// 소개 관리 테이블
			
		     
		     default     : logger.info("등록 되지 않은 테이블입니다."); return null; 	// 잘못됫 입력 정보  null 리턴
	    }
		return TableName;
	}
	
	/**
	 * 검색조건으로 받아온 값을 각 테이블의 컬럼명으로 변환 
	 * @param name 
	 * */
	public String[] setConditions(String searchCondition, String name) {
		// TODO Auto-generated method stub
		String names[]=searchCondition.split(",");
		for(int i=0;i<names.length;i++){
			names[i]=name +"_"+names[i];
		}
		return names;
	}
	
	/**
	 * 받아온 keyno값을 테이블 규칙에 맞게 변경
	 * ex) 123 -> SC_0000000123
	 */
	public String getKeyno(String keyno,String tableName){
		int length=keyno.length();
		for(int i=length;i<10;i++){
			keyno="0"+keyno;
		}
		return tableName+"_"+keyno;
		
	}
	
}
