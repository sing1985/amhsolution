package com.amhsolution.common.page;

import javax.servlet.ServletContext;
import org.springframework.web.context.ServletContextAware;
import egovframework.rte.ptl.mvc.tags.ui.pagination.AbstractPaginationRenderer;

/**
 * @Pagination
 * Jsp 화면 페이지 처리 UI 생성 클래스 (관리자)
 * @author 신희원
 * @version 1.0
 * @since 2014-11-14
 */
public class AdminPagination extends AbstractPaginationRenderer implements ServletContextAware{
	public AdminPagination() {
	} 
	
	public void initVariables(){
		firstPageLabel	  = "";
        previousPageLabel = "<li><a href=\"?pageIndex={1}\"  onclick=\"{0}({1});return false; \"><i class=\"fa fa-arrow-left\"></i></a></li>&#160;";
        currentPageLabel  = "<li class=\"active\"><a href=\"javascript:;\">{0}</a></li>&#160;";
        otherPageLabel    = "<li><a href=\"?pageIndex={1}\" class=\"num\" onclick=\"{0}({1});return false; \">{2}</a></li>&#160;";
        nextPageLabel     = "<li><a href=\"?pageIndex={1}\"  onclick=\"{0}({1});return false; \"><i class=\"fa fa-arrow-right\"></i></a></li>&#160;";
        lastPageLabel	  = "";
	} 
	
	public void setServletContext(ServletContext servletContext){
		initVariables();
	} 

}

