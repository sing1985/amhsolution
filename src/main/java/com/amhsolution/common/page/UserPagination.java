package com.amhsolution.common.page;

import javax.servlet.ServletContext;
import org.springframework.web.context.ServletContextAware;
import egovframework.rte.ptl.mvc.tags.ui.pagination.AbstractPaginationRenderer;

/**
 * @Pagination
 * Jsp 화면 페이지 처리 UI 생성 클래스 (관리자)
 * @author 신희원
 * @version 1.0
 * @since 2014-11-14
 */
public class UserPagination extends AbstractPaginationRenderer implements ServletContextAware{
	public UserPagination() {
	} 
	public void initVariables(){
		firstPageLabel    = "<a href=\"?pageIndex={1}\"  onclick=\"{0}({1});return false; \">처음</a>&#160;";
        previousPageLabel = "<a href=\"?pageIndex={1}\"  onclick=\"{0}({1});return false; \">이전</a>&#160;";
        currentPageLabel  = "<strong>{0}</strong>&#160;";
        otherPageLabel    = "<a href=\"?pageIndex={1}\" class=\"num\" onclick=\"{0}({1});return false; \">{2}</a>&#160;";
        nextPageLabel     = "<a href=\"?pageIndex={1}\"  onclick=\"{0}({1});return false; \">다음</a>&#160;";
        lastPageLabel     = "<a href=\"?pageIndex={1}\"  onclick=\"{0}({1});return false; \">마지막</a>&#160;";
	} 
	
	public void setServletContext(ServletContext servletContext){
		initVariables();
	} 

}

