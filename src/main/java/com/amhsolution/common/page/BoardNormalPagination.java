package com.amhsolution.common.page;

import javax.servlet.ServletContext;
import org.springframework.web.context.ServletContextAware;
import egovframework.rte.ptl.mvc.tags.ui.pagination.AbstractPaginationRenderer;

/**
 * @Pagination
 * Jsp 화면 페이지 처리 UI 생성 클래스 (게시판) 
 * @author 신희원
 * @version 1.0
 * @since 2014-11-14
 */
public class BoardNormalPagination extends AbstractPaginationRenderer implements ServletContextAware{
	public BoardNormalPagination() {
	} 
	
	public void initVariables(){
		firstPageLabel    = ""; 
        previousPageLabel = "<a href=\"?pageIndex={1}\"  onclick=\"{0}({1});return false; \"><img src=\"/resources/asia/common/images/prev_arrow_blue.png\" /></a>&#160;";
        currentPageLabel  = "<div class=\"now-page\">{0}&#160;</div>";
        otherPageLabel    = "<div><a href=\"?pageIndex={1}\" onclick=\"{0}({1});return false; \">{2}</a></div>&#160;";
        nextPageLabel     = "<a href=\"?pageIndex={1}\"  onclick=\"{0}({1});return false; \"><img src=\"/resources/asia/common/images/next_arrow_blue.png\" /></a>&#160;";
        lastPageLabel     = "";
	} 
	
	public void setServletContext(ServletContext servletContext){
		initVariables();
	} 

}

