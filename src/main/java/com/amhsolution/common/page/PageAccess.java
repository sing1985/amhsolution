package com.amhsolution.common.page;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amhsolution.common.component.CommonInterface;

import egovframework.rte.fdl.cmmn.AbstractServiceImpl;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * @PageAccess
 * 페이징 처리를 담당한다.
 * PaginationInfo 클래스에 갱신하기위해
 * 페이지 처리를 원하는 페이지의 현재 페이지 번호와 전체 데이터 개수를 조회하는 쿼리문을 파라미터 값으로 받는다.
 * @author 신희원
 * @version 1.0
 * @since 2014-11-14
 */
@Service("PageAccess")
public class PageAccess extends AbstractServiceImpl{
	
	/** Properties 설정 파일 읽기 */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** 공통 컴포넌트 */
	@Autowired CommonInterface Component;
	
	/**
	 * 페이지 계산 처리
	 * @throws Exception
	 */
	public PaginationInfo getPagInfo(Integer pageIndex, String query) throws Exception{
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(pageIndex);
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));
		paginationInfo.setTotalRecordCount(Component.getCount(query));
		return paginationInfo;
	}
	
	/**
	 * 페이지 계산 처리 (파라미터 사용)
	 * @throws Exception
	 */
	public PaginationInfo getPagInfo(Integer pageIndex, String query, Object obj) throws Exception{
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(pageIndex);
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));
		paginationInfo.setTotalRecordCount(Component.getCount(query, obj));
		return paginationInfo;
	}
	
	
}