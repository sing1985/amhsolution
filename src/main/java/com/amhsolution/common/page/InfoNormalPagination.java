package com.amhsolution.common.page;

import javax.servlet.ServletContext;
import org.springframework.web.context.ServletContextAware;
import egovframework.rte.ptl.mvc.tags.ui.pagination.AbstractPaginationRenderer;

/**
 * @Pagination
 * Jsp 화면 페이지 처리 UI 생성 클래스 (알림마당) 
 * @author 이재령
 * @version 1.0
 * @since 2016-05-24
 */
public class InfoNormalPagination extends AbstractPaginationRenderer implements ServletContextAware{
	public InfoNormalPagination() {
	} 
	
	public void initVariables(){
		firstPageLabel    = ""; 
        previousPageLabel = "<a href=\"?pageIndex={1}\"  onclick=\"{0}({1});return false; \"><img src=\"/resources/common/img/board_prev.jpg\" /></a>&#160;";
        currentPageLabel  = "<div class=\"now-page\">{0}&#160;</div>";
        otherPageLabel    = "<div><a href=\"?pageIndex={1}\" onclick=\"{0}({1});return false; \">{2}</a></div>&#160;";
        nextPageLabel     = "<a href=\"?pageIndex={1}\"  onclick=\"{0}({1});return false; \"><img src=\"/resources/common/img/board_next.jpg\" /></a>&#160;";
        lastPageLabel     = "";
	} 
	
	public void setServletContext(ServletContext servletContext){
		initVariables();
	} 

}

