package com.amhsolution.common.file;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import com.amhsolution.common.component.CommonInterface;
import com.amhsolution.common.file.dto.FileSub;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.property.EgovPropertyService;

/**
 * @FileDownloadTools
 * @Service : FileDownloadTools
 * 공통기능의 다운로드를 관리 하는 툴 클래스 
 * @author 신희원 
 * @version 1.0
 * @since 2014-11-12
 */
@Service("FileDownloadTools")
public class FileDownloadTools extends EgovAbstractServiceImpl{
	
	private static final Logger logger = LoggerFactory.getLogger(FileDownloadTools.class);
	/** 프로퍼티 정보 */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    /** 공통 컴포넌트 */
	@Autowired CommonInterface Component;
    
    /**
     * 브라우저 구분 얻기.
     * 
     * @param request
     * @return
     */
    private String getBrowser(HttpServletRequest request) {
        String header = request.getHeader("User-Agent");
        if (header.indexOf("MSIE") > -1 || header.indexOf("Trident") > -1) {
            return "MSIE";
        } else if (header.indexOf("Chrome") > -1) {
            return "Chrome";
        } else if (header.indexOf("Opera") > -1) {
            return "Opera";
        }
        return "Firefox";
    }  

    /**
     * Disposition 지정하기.
     * @param filename
     * @param request
     * @param response
     * @throws Exception
     */
    private void setDisposition(String filename, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	String browser = getBrowser(request);
		String dispositionPrefix = "attachment; filename=";
		String encodedFilename = null;
	
	if (browser.equals("MSIE")) {
	    encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
	} else if (browser.equals("Firefox")) {
	    encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
	} else if (browser.equals("Opera")) {
	    encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
	} else if (browser.equals("Chrome")) {
	    StringBuffer sb = new StringBuffer();
	    for (int i = 0; i < filename.length(); i++) {
		char c = filename.charAt(i);
		if (c > '~') {
		    sb.append(URLEncoder.encode("" + c, "UTF-8"));
		} else {
		    sb.append(c);
		}
	    }
	    encodedFilename = sb.toString();
	} else {
	    //throw new RuntimeException("Not supported browser");
	    throw new IOException("Not supported browser");
	}
	dispositionPrefix=dispositionPrefix.replaceAll("\r", "").replaceAll("\n", "");
	encodedFilename=encodedFilename.replaceAll("\r", "").replaceAll("\n", "");
	response.setHeader("Content-Disposition", dispositionPrefix + encodedFilename);

		if ("Opera".equals(browser)){
		    response.setContentType("application/octet-stream;charset=UTF-8");
		}
    }

    
    /**
     * 첨부파일로 등록된 파일에 대하여 다운로드를 제공한다.
     * @param commandMap
     * @param response
     * @throws Exception
     */
    public void FileDownload(FileSub FileSub, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	Component.updateData("File.AFS_FileDownCouting", FileSub);
    	/** 경로 설정 */
		String uploadPath = propertiesService.getString("FilePath")+ FileSub.getFS_FOLDER(); 
	    File uFile = new File(uploadPath, FilenameUtils.getName(FileSub.getFS_CHANGENM() + "." + FileSub.getFS_EXT()));
	    int fSize = (int)uFile.length();
	    if (fSize > 0) {
			String mimetype = "application/x-msdownload";
			System.out.println(FileSub.getFS_ORINM());
			setDisposition(FileSub.getFS_ORINM(), request, response);
			response.setContentLength(fSize);
			BufferedInputStream in = null;
			BufferedOutputStream out = null;

		try {
			    in = new BufferedInputStream(new FileInputStream(uFile));
			    out = new BufferedOutputStream(response.getOutputStream());
			    // MAC Safari브라우져에서 확장자exe 추가되는 문제로 Tika로 mime-type 선언
			    Tika tika = new Tika();
			    mimetype = tika.detect(in);
			    response.setContentType(mimetype);
			    //System.err.println(mimetype+"################################ : TIKA MIME-TYPE");
			    FileCopyUtils.copy(in, out);
			    out.flush();
		} catch (IOException ex) {
			logger.debug("IO 에러");
		} finally {
		    if (in != null) {
			    in.close();
		    }
		    if (out != null) {
			    out.close();
		    }
		}  
	    }/* else { 
			response.setContentType("application/x-msdownload");
			PrintWriter printwriter = response.getWriter();
			printwriter.println("<html>");
				printwriter.println("<br><br><br><h2>파일이 손상되었거나 존재 하지 않습니다. 파일명: <br>" + FileSub.getFS_ORINM() + "</h2>");
				printwriter.println("<br><br><br><center><h3><a href='javascript:;' onclick='history.go(-2)'>뒤로가기</a></h3></center>");
			printwriter.println("</html>");
			printwriter.flush();
			printwriter.close();
	    }*/
    }
    
}
