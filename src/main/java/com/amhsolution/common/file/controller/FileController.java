package com.amhsolution.common.file.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amhsolution.common.component.CommonInterface;
import com.amhsolution.common.file.FileDownloadTools;
import com.amhsolution.common.file.FileUploadTools;
import com.amhsolution.common.file.dto.FileSub;

/**
 * 
 * @FileName: FileController.java
 * @Project : SafetheFood
 * @Date    : 2016. 12. 21. 
 * @Author  : 이재령
 * @Version :
 */
@Controller
public class FileController {
	
	Logger log = Logger.getLogger(this.getClass());
     
	/** 공통 컴포넌트 */
	@Autowired CommonInterface Component;
	
	/** 파일다운로드 툴*/
	@Autowired private FileDownloadTools FileDownloadTools;
  
	/** 파일업로드 툴*/
	@Autowired private FileUploadTools FileUploadTools;
	
	
	/**
	 * 파일 업로드 처리
	 * @param req
	 * @param FS_FM_KEYNO
	 * @throws Exception
	 */
	@RequestMapping(value = "/async/MultiFile/uploadAjax.do")
	@ResponseBody
	public void MultiFileUpLoad(HttpServletRequest req
//		, @RequestParam(value="FS_FM_KEYNO",required = false) String FS_FM_KEYNO
		,String FS_FM_KEYNO
		) throws Exception{ 
		//등록자
		if(req.getSession().getAttribute("userInfo")!=null){
			
		    @SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>) req.getSession().getAttribute("userInfo");
			
			if(map.get("UI_ID").toString() != null && !map.get("UI_ID").toString().equals("")){
				String User = map.get("UI_ID").toString();
				FileUploadTools.FileUpload(req, FS_FM_KEYNO, User);
			}else{
				throw new NullPointerException();
			} 
		}
		else{
			throw new NullPointerException();
		}
	}	
	
}
