package com.amhsolution.common.file;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.amhsolution.common.component.CommonInterface;
import com.amhsolution.common.file.dto.FileMain;
import com.amhsolution.common.file.dto.FileSub;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.property.EgovPropertyService;

/**
 * @FileUploadTools
 * @Service : FileUploadTools
 * 공통기능의 파일 업로드를 관리 하는 툴 클래스 
 * @author 신희원
 * @version 1.0
 * @since 2014-11-12
 */

@Service("FileUploadTools")
public class FileUploadTools extends EgovAbstractServiceImpl{
	
	private static final Logger logger = LoggerFactory.getLogger(FileUploadTools.class);
	
	/** 프로퍼티 정보 읽기 */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	
	/** 공통 컴포넌트 */
	@Autowired CommonInterface Component;
	
	
	//하나의 폴더당 내부 파일의 개수
	final private int FOLDER_FILE_CNT = 50;
	
	
	
	/**
 	 * 다중 파일을 업로드 한다.
 	 * @param request 요청
 	 * @param FS_FM_KEYNO 메인코드 키
 	 * @param REGNM 등록자
 	 * @throws Exception
 	 */
	public FileSub FileUpload(HttpServletRequest request, String FS_FM_KEYNO, String REGNM) {
		
		System.out.println("업로드!!!");
		
		FileMain FileMain = new FileMain();
		FileMain.setFM_KEYNO(FS_FM_KEYNO);
		FileMain.setFM_REGNM(REGNM);
		
		//메인코드 확인 및 등록 처리
		int result = Component.getCount("File.AFM_MainFileChecking", FileMain);
		if(result == 0){
			Component.createData("File.AFM_FileInfoInsert",FileMain);
		}
		 
		final MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
		final Map<String, MultipartFile> files = multiRequest.getFileMap();

		//프로퍼티 경로 불러오기
		String propertiespath = propertiesService.getString("FilePath");
		
		FileSub FileSub = new FileSub();
		
		//확장자
		String FS_EXT = "";
		//원본파일명
		String FS_ORINM = "";
		//파일사이즈
		String FS_SIZE = "";
		//변경 파일명
		String FS_CHANGENM = "";
		
		FileSub.setFS_FM_KEYNO(FS_FM_KEYNO);
		FileSub.setFS_KEYNO(Component.getTableKey("FS"));
		FileSub.setFS_FOLDER(SaveFolder(propertiespath));
		
		/** 경로 설정 */
		String uploadPath = propertiespath + FileSub.getFS_FOLDER();
		
		/** 폴더 생성 */ 
		Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
		
		/** 파일 저장 처리 */  
		while (itr.hasNext()){
			System.out.println("저장처리");
			Entry<String, MultipartFile> entry = itr.next();
			MultipartFile file = entry.getValue();
			if(file.getOriginalFilename() != null && !file.getOriginalFilename().equals("")){
				/** 원본파일명 취득 */
				FS_ORINM = file.getOriginalFilename();
				/** 확장자 취득 */
				FS_EXT = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.')+1, file.getOriginalFilename().length());
				/** 사이즈 취득 */
				FS_SIZE = Long.toString(file.getSize());
				/** 파일명 변환 후 저장*/
				FS_CHANGENM = setfilename();
				String filePath = uploadPath + FilenameUtils.getName(FS_CHANGENM + "." + FS_EXT);
				try {
					file.transferTo(new File(filePath));
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		//파일 정보 저장
		FileSub.setFS_FILE_SIZE(FS_SIZE);
		FileSub.setFS_EXT(FS_EXT);
		FileSub.setFS_ORINM(FS_ORINM);
		FileSub.setFS_CHANGENM(FS_CHANGENM);
		Component.createData("File.AFS_FileInfoInsert", FileSub);
		return FileSub;
	}
	
	
	
	
	/**
 	 * DATA URI 데이터 기반으로 업로드 합니다
 	 * @param imgInfo	해쉬맵으로 uri,title,ext 정보를 담고있음
 	 * @param FS_FM_KEYNO 메인코드 키
 	 * @param REGNM 등록자
 	 * @throws Exception
 	 */
	public FileSub FileUploadByDataURI(HashMap<String, String> imgInfo, String FS_FM_KEYNO, String REGNM) {
		
		
		
		System.out.println("데이터 기반으로 업로드");
		
		FileMain FileMain = new FileMain();
		FileMain.setFM_KEYNO(FS_FM_KEYNO);
		FileMain.setFM_REGNM(REGNM);
		
		//메인코드 확인 및 등록 처리
		int result = Component.getCount("File.AFM_MainFileChecking", FileMain);
		if(result == 0){
			Component.createData("File.AFM_FileInfoInsert",FileMain);
		}
		
		//프로퍼티 경로 불러오기
		String propertiespath = propertiesService.getString("FilePath");
		
		FileSub FileSub = new FileSub();
		FileSub.setFS_KEYNO(Component.getTableKey("FS"));
		FileSub.setFS_FM_KEYNO(FS_FM_KEYNO);
		
		String uri = imgInfo.get("uri");
		String title = imgInfo.get("title");
		String ext = imgInfo.get("ext");
		
		//확장자
		String FS_EXT = "";
		//파일사이즈
		String FS_SIZE = "";
		//변경 파일명
		String FS_CHANGENM = "";
		
		FileSub.setFS_FOLDER(SaveFolder(propertiespath));
		
		/** 경로 설정 */
		String uploadPath = propertiespath + FileSub.getFS_FOLDER();
	
		System.out.println("저장처리");
		
		/** 확장자 취득 */
		FS_EXT = ext;
		/** 파일명 변환 후 저장*/
		FS_CHANGENM = setfilename();
		String filePath = uploadPath + FilenameUtils.getName(FS_CHANGENM + "." + FS_EXT);
		try {
			 byte[] imagedata = DatatypeConverter.parseBase64Binary(uri.substring(uri.indexOf(",") + 1));
			 BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(imagedata));
			 File file = new File(filePath);
		     ImageIO.write(bufferedImage, FS_EXT, file);
			 /** 사이즈 취득 */
		     FS_SIZE = file.length()+"";
		} catch (Exception e) {
			// TODO: handle exception
		}
		//파일 정보 저장
		FileSub.setFS_FILE_SIZE(FS_SIZE);
		FileSub.setFS_EXT(FS_EXT);
		FileSub.setFS_ORINM(title + "." + FS_EXT);
		FileSub.setFS_CHANGENM(FS_CHANGENM);
		
		Component.createData("File.AFS_FileInfoInsert", FileSub);	
		
		
			
		return FileSub;
	}
	

	
	/**
 	 * 순번에 따른 파일을 업로드 한다.
 	 * @param request 요청
 	 * @param FS_FM_KEYNO 메인코드 키
 	 * @param REGNM 등록자
 	 * @throws Exception
 	 */
	public FileSub FileUpload(HttpServletRequest request, String FS_FM_KEYNO, String REGNM, int cnt) throws Exception {
		
		
		System.out.println("업로드!!!");
		
		
		FileMain FileMain = new FileMain();
		FileMain.setFM_KEYNO(FS_FM_KEYNO);
		FileMain.setFM_REGNM(REGNM);
		//메인코드 확인 및 등록 처리
		int result = Component.getCount("File.AFM_MainFileChecking", FileMain);
		if(result == 0){
			Component.createData("File.AFM_FileInfoInsert",FileMain);
		}
		final MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
		final Map<String, MultipartFile> files = multiRequest.getFileMap();
		//프로퍼티 경로 불러오기
		String propertiespath = propertiesService.getString("FilePath");
		
		FileSub FileSub = new FileSub();
		
		//확장자
		String FS_EXT = "";
		//원본파일명
		String FS_ORINM = "";
		//파일사이즈
		String FS_SIZE = "";
		//변경 파일명
		String FS_CHANGENM = "";
		System.out.println("업로드!!!");
		
		FileSub.setFS_FM_KEYNO(FS_FM_KEYNO);
		FileSub.setFS_KEYNO(Component.getTableKey("FS"));
		FileSub.setFS_FOLDER(SaveFolder(propertiespath));
		
		/** 경로 설정 */
		String uploadPath = propertiespath + FileSub.getFS_FOLDER();
		
		/** 폴더 생성 */ 
		Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
		
		int Count = 1;
		
		/** 파일 저장 처리 */  
		while (itr.hasNext()){
			System.out.println(Count +"," + cnt);
			Entry<String, MultipartFile> entry = itr.next();
			MultipartFile file = entry.getValue();
			System.out.println(file.getOriginalFilename());
			
			if(Count == cnt){
				System.out.println("저장처리");
				if(file.getOriginalFilename() != null && !file.getOriginalFilename().equals("")){
					/** 원본파일명 취득 */
					FS_ORINM = file.getOriginalFilename(); 
					System.out.println(FS_ORINM);
					/** 확장자 취득 */
					FS_EXT = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.')+1, file.getOriginalFilename().length());
					/** 사이즈 취득 */
					FS_SIZE = Long.toString(file.getSize());
					/** 파일명 변환 후 저장*/
					FS_CHANGENM = setfilename();
					String filePath = uploadPath + FilenameUtils.getName(FS_CHANGENM + "." + FS_EXT);
					file.transferTo(new File(filePath));
				}
			}
			Count++;
		}
		//파일 정보 저장
		FileSub.setFS_FILE_SIZE(FS_SIZE);
		FileSub.setFS_EXT(FS_EXT);
		FileSub.setFS_ORINM(FS_ORINM);
		FileSub.setFS_CHANGENM(FS_CHANGENM);
		Component.createData("File.AFS_FileInfoInsert", FileSub);
		return FileSub;
	}
	
	/**
	 * 리사이즈 - 850 * 436
 	 * 순번에 따른 파일을 업로드 한다.
 	 * height 0 일때는 height값 autoSizing
 	 * @param request 요청
 	 * @param FS_FM_KEYNO 메인코드 키
 	 * @param REGNM 등록자
 	 * @throws Exception
 	 */
	public FileSub resizeFileUpload(HttpServletRequest request, String FS_FM_KEYNO, String REGNM, int cnt,int width,int height) throws Exception {
		 
		FileMain FileMain = new FileMain();
		FileMain.setFM_KEYNO(FS_FM_KEYNO);
		FileMain.setFM_REGNM(REGNM);
		
		//메인코드 확인 및 등록 처리
		int result = Component.getCount("File.AFM_MainFileChecking", FileMain);
		if(result == 0){
			Component.createData("File.AFM_FileInfoInsert",FileMain);
		}
		 
		final MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
		final Map<String, MultipartFile> files = multiRequest.getFileMap();

		//프로퍼티 경로 불러오기
		String propertiespath = propertiesService.getString("FilePath");
		
		FileSub FileSub = new FileSub();
		
		//확장자
		String FS_EXT = "";
		//원본파일명
		String FS_ORINM = "";
		//파일사이즈
		String FS_SIZE = "";
		//변경 파일명
		String FS_CHANGENM = "";
		
		//리사이즈 후 파일명
		String RE_FS_CHANGENM = "";
		//리사이즈 후 파일사이즈
		String RE_FS_SIZE = "";
		
		FileSub.setFS_FM_KEYNO(FS_FM_KEYNO);
		FileSub.setFS_KEYNO(Component.getTableKey("FS"));
		FileSub.setFS_FOLDER(SaveFolder(propertiespath));
		
		/** 경로 설정 */
		String uploadPath = propertiespath + FileSub.getFS_FOLDER();
		
		/** 폴더 생성 */ 
		Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
		
		int Count = 1;
		
		/** 파일 저장 처리 */  
		while (itr.hasNext()){
			System.out.println(Count +"," + cnt);
			Entry<String, MultipartFile> entry = itr.next();
			MultipartFile file = entry.getValue();
			System.out.println(file.getOriginalFilename());
			
			if(Count == cnt){
				System.out.println("저장처리");
				if(file.getOriginalFilename() != null && !file.getOriginalFilename().equals("")){
					/** 원본파일명 취득 */
					FS_ORINM = file.getOriginalFilename(); 
					System.out.println(FS_ORINM);
					/** 확장자 취득 */
					FS_EXT = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.')+1, file.getOriginalFilename().length());
					/** 사이즈 취득 */
					FS_SIZE = Long.toString(file.getSize());
					/** 파일명 변환 후 저장*/
					FS_CHANGENM = setfilename();
					String filePath = uploadPath + FilenameUtils.getName(FS_CHANGENM + "." + FS_EXT);
					file.transferTo(new File(filePath));
					
					/** 리사이즈 */
					javaxt.io.Image image = new javaxt.io.Image(filePath);
					if(height == 0){
						height=width * image.getHeight() / image.getWidth();
					} 
					if(image.getWidth() > width || image.getHeight() >= height){ 
			    		image.resize(width, height);     
			    		byte[] img = image.getByteArray("jpg");
			    		FileOutputStream dos=null;
			    		try{
			    			dos = new FileOutputStream(filePath); 
			    			dos.write(img);  
			    		} catch(IOException ex){
	    			logger.debug("쓰기 에러");
			    		}finally {
			    			dos.close();
						}
		    		} 
					File newFile = new File(filePath);
					FS_SIZE = Long.toString(newFile.length());
//					
				}
			}
			Count++;
		}
		//파일 정보 저장
		FileSub.setFS_FILE_SIZE(FS_SIZE); 
		FileSub.setFS_EXT(FS_EXT);
		FileSub.setFS_ORINM(FS_ORINM);
		FileSub.setFS_CHANGENM(FS_CHANGENM);
		Component.createData("File.AFS_FileInfoInsert", FileSub);
		return FileSub;
	}
	
	
	
	
	
	
	
	
	/**
 	 * 모바일 이미지 파일 사이즈 변경
 	 * @param Filepath
 	 * @throws Exception
 	 */
 	private void ImgResize(String Filepath,String m_Filepath,int width)throws Exception{
 		javaxt.io.Image image = new javaxt.io.Image(Filepath);
 		int imageWidth = width;
 		int imageHeight= imageWidth * image.getHeight() / image.getWidth();
		 image.resize(imageWidth, imageHeight); 
		 byte[] img = image.getByteArray("jpg");
         FileOutputStream dos=null;
 		 try{
 			dos = new FileOutputStream(m_Filepath); 
 			dos.write(img);  
 		 } catch(IOException ex){
 			logger.debug("쓰기 에러");
 		 }finally {
 			dos.close();
		 }
 	}
	
	/**
 	 * 업로드시 폴더 여부 확인 및 생성
 	 * @param Uploadpath
 	 */
	public String SaveFolder(String Uploadpath){
		//System.out.println("접근 폴더 : " + Uploadpath);
		String folderPath = "";
		String RtnValue = "";
 		File saveFolder = new File(Uploadpath);
		if (!saveFolder.exists() || saveFolder.isFile()) {
				saveFolder.mkdirs();
		}
		
		File FilegetCnt = new File(Uploadpath); 
		String Cntfiles[] = FilegetCnt.list();	
		if(Cntfiles.length == 0){
			File FilesaveFolder = new File(Uploadpath +"1/"); 
			if (!FilesaveFolder.exists() || FilesaveFolder.isFile()) {
				FilesaveFolder.mkdirs();  
			}
			folderPath = Uploadpath +"1/";
			RtnValue = "1/";
		}else if(Cntfiles.length > 0){
			boolean FolderCheck = false;
			Loop:
			for(int i = 1; i <= Cntfiles.length; i++){
				File CntToT = new File(Uploadpath + i +"/"); 
				//System.out.println(Uploadpath + i +"/");
				if(CntToT.list().length < FOLDER_FILE_CNT){
					folderPath = Uploadpath +  i + "/";
					RtnValue = i + "/";
					FolderCheck = true;
					break Loop;
				}
			}if(FolderCheck == false){
				folderPath = Uploadpath + (Cntfiles.length + 1) + "/";
				RtnValue = (Cntfiles.length + 1) + "/";
				File NewFolderCreate = new File(folderPath); 
				if (!NewFolderCreate.exists() || NewFolderCreate.isFile()) {
					NewFolderCreate.mkdirs();
				}
			}  
		}
		return RtnValue;
 	}
 	
	
	
	 /**
	  * 파일 업로드 내부의 고유키 부여
	  * @comment
	  * 2016.04.08. SooAn
	  *  웹에디터에서 이미지의 원활한 수정을 위해서
	  *  100글자에서 10글자로 대폭 축소시킴.
	  *  예측 위험발생가능성 - 폴더 당 최대생성하는 파일갯수 50회 중 1/36^15 의 확률로 발생 추측
	  * @return
	  */ 
	public String setfilename(){
			SecureRandom rnd =new SecureRandom();
			String buf =""; 
			for(int i=0 ; i < 15 ; i++){  
			    if(rnd.nextBoolean()){ 
			        buf +=((char)((int)(rnd.nextInt(26))+97)); 
			    }else{
			        buf+=((rnd.nextInt(10))); 
			    }
			}
	        return buf;
	 	}
	
	/**
	 * 이미지 변경 메소드
	 * @param FS_KEYNO
	 * @param req
	 * @param cnt 넘어온 파일 정보중 몇번째 파일인가
	 * @param resize 리사이즈 된 이미지 있는지 여부 
	 * @return
	 * @throws Exception
	 */
	public FileSub imageChange(String FS_KEYNO, HttpServletRequest req,String ID,int cnt,boolean resize,int width,int height) throws Exception{
		System.out.println("이미지 수정! ");
		FileSub FileSub = new FileSub();
		FileSub.setFS_KEYNO(FS_KEYNO);
		FileSub=Component.getData("File.AFS_SubFileDetailselect", FileSub);
		String FM_KEY = "";
		
		if(FileSub != null){
			//기존의 이미지 삭제
			UpdateFileDelete(FS_KEYNO);
			FM_KEY = FileSub.getFS_FM_KEYNO();
		}else{
			FM_KEY = Component.getTableKey("FM");
			
		}
		
		//새로운 이미지 등록
		if(resize && width == 0 && height == 0){ // 이미지 저장후 모바일용 리사이즈 이미지 하나더 저장
			//FileSub = FileUpload(req, FileSub.getFS_FM_KEYNO(), ID, cnt,true);		
		}else if(resize && width != 0){ // 리사이즈 한 파일만 저장
			FileSub = resizeFileUpload( req, FileSub.getFS_FM_KEYNO(), ID, cnt,width,height);
		}else{ // 리사이즈 안함
			FileSub = FileUpload(req, FileSub.getFS_FM_KEYNO(), ID, cnt);
		}
		
		return FileSub;
	}
	
	
	
	
	/**
	 * 업로드 파일 삭제 
	 */
	public void UpdateFileDelete(String file) {
		String[] deletefile = file.split(",");
		String uploadPath = propertiesService.getString("FilePath");
		for(int i=0; i<deletefile.length; i++) {
			FileSub fileSub = new FileSub();
			fileSub.setFS_KEYNO(deletefile[i]);
			//파일삭제처리
			fileSub = Component.getData("File.AFS_SubFileDetailselect", fileSub);
			
			if(fileSub != null){
				String fullPath = uploadPath+fileSub.getFS_FOLDER()+FilenameUtils.getName(fileSub.getFS_CHANGENM()+"."+fileSub.getFS_EXT());
				File dFile = new File(fullPath);
				if(dFile.exists()){
					dFile.delete();
				}
				//DB정보 삭제처리
				Component.deleteData("File.AFS_FileUploadDelete", fileSub);
			}
		}
	}
	
	/**
	 * 파일 확장자 및 용량 체크
	 * 조건에 맞으면 true 틀리면 false
	 */
public boolean checkEXT_SIZE(HttpServletRequest request, String ext, int size) throws Exception {
		
		final MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
		final Map<String, MultipartFile> files = multiRequest.getFileMap();

		String EXT[] = ext.split(",");
		boolean extEquals =false; // 확장자 일치 여부
		
		Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
		while (itr.hasNext()){
			Entry<String, MultipartFile> entry = itr.next();
			MultipartFile file = entry.getValue();
			if(file.getOriginalFilename() != null && !file.getOriginalFilename().equals("")){
				/** 확장자 취득 */
				String FS_EXT = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.')+1, file.getOriginalFilename().length());
				/** 사이즈 취득 */
				long FS_SIZE = file.getSize();
				
				for(String e : EXT) 
					if(e.equals(FS_EXT)) extEquals=true;
				if ( size < FS_SIZE/1024/1024 ) return false;
			}
		}
		
		return extEquals;
	}
	
	
}
