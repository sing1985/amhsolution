package com.amhsolution.common.file.dto;

import java.io.Serializable;


/**
 * @FileMain
 * 공통기능의 상위 파일정보를 관리 하는 빈즈
 * @author 신희원
 * @version 1.0
 * @since 2014-11-12
 */
public class FileMain extends FileSub implements Serializable {
	
	//시리얼 넘버
	static final long serialVersionUID = 42L;
	
	//고유키
	private String FM_KEYNO = null;
	
	//업로드일자
	private String FM_REGDT = null;
	
	//업로드 사용자
	private String FM_REGNM = null;
	

	
	public String getFM_REGDT() {
		return FM_REGDT;
	}
	public void setFM_REGDT(String fM_REGDT) {
		FM_REGDT = fM_REGDT;
	}
	public String getFM_REGNM() {
		return FM_REGNM;
	}
	public void setFM_REGNM(String fM_REGNM) {
		FM_REGNM = fM_REGNM;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getFM_KEYNO() {
		return FM_KEYNO;
	}
	public void setFM_KEYNO(String fM_KEYNO) {
		FM_KEYNO = fM_KEYNO;
	}	
	
	
}
