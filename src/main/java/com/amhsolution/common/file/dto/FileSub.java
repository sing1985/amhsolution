package com.amhsolution.common.file.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @FileSub
 * 공통기능의 하위 파일정보내용을 관리 하는 빈즈
 * @author 신희원
 * @version 1.0
 * @since 2014-11-12
 */
public class FileSub implements Serializable {
	
	//고유키
	private String FS_KEYNO = null;
	
	//상위 그룹키
	private String FS_FM_KEYNO = null;
	
	//파일크기
	private String FS_FILE_SIZE = null;
	
	//원본파일명칭
	private String FS_ORINM = null;
	
	//코드화 완료된 파일명칭
	private String FS_CHANGENM = null;
	
	//확장자
	private String FS_EXT = null;
	
	//저장위치 절대경로
	private String FS_FOLDER = null;
	

	public String getFS_KEYNO() {
		return FS_KEYNO;
	}

	public void setFS_KEYNO(String fS_KEYNO) {
		FS_KEYNO = fS_KEYNO;
	}

	public String getFS_FM_KEYNO() {
		return FS_FM_KEYNO;
	}

	public void setFS_FM_KEYNO(String fS_FM_KEYNO) {
		FS_FM_KEYNO = fS_FM_KEYNO;
	}

	public String getFS_FILE_SIZE() {
		return FS_FILE_SIZE;
	}

	public void setFS_FILE_SIZE(String fS_FILE_SIZE) {
		FS_FILE_SIZE = fS_FILE_SIZE;
	}

	public String getFS_ORINM() {
		return FS_ORINM;
	}

	public void setFS_ORINM(String fS_ORINM) {
		FS_ORINM = fS_ORINM;
	}

	public String getFS_CHANGENM() {
		return FS_CHANGENM;
	}

	public void setFS_CHANGENM(String fS_CHANGENM) {
		FS_CHANGENM = fS_CHANGENM;
	}

	public String getFS_EXT() {
		return FS_EXT;
	}

	public void setFS_EXT(String fS_EXT) {
		FS_EXT = fS_EXT;
	}

	public String getFS_FOLDER() {
		return FS_FOLDER;
	}

	public void setFS_FOLDER(String fS_FOLDER) {
		FS_FOLDER = fS_FOLDER;
	}	
	
}
