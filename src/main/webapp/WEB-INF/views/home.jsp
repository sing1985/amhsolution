<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>

<style>
  #Contents {width:100% !important; padding:0 !important; min-height:150px !important;}
  .data_list tr:HOVER td {cursor:pointer;}
  .home_miribogi {width:31%; height: 125px; float:left; margin:25px 1%}
  .home_miribogi .home_barogagi_box {width:50%; height:50px; float:left; cursor:pointer; }
  .home_miribogi .home_barogagi_box img {width:100%; height:100%; display:block;}
  .header_gradient {display:none !important;}
  
  #header_bottom{
    background: url(/resources/css/images/common/main_01.jpg) center center;
    background-size: 1100px;
    height: 285px;
  }
  
  .con_top {font-size:16px; font-weight:bold; color:#4b4444;}
  
</style>

      
<%-- <div class="home_miribogi">
  <div class="con_top"><img src=""><span>공지사항</span></div>
	<table class="data_list">
	 <colgroup>
	   <col width="100%">
	 </colgroup>
	  <c:forEach items="${resultList }" var="data" end="2">
	     <tr onclick="detailLink('cs', 'cs1', '${data.B_NT_KEYNO}')">
	       <td class="tdR">&nbsp;${data.B_NT_TITLE }</td>
	     </tr>
	   </c:forEach>
	   <c:if test="${fn:length(resultList) eq 0 }">
	     <tr>
	       <td colspan="4" style="text-align:center;"><b>목록이 없습니다.</b></td>
	     </tr>
	    </c:if>
	</table>
</div> --%>

<%-- <div class="home_miribogi">
<div class="con_top"><img src=""><span>문의게시판</span></div>
<colgroup>
     <col width="100%">
   </colgroup>
<table class="data_list">
  <c:forEach items="${resultList2 }" var="data" end="2">
     <tr onclick="detailLink('cs', 'cs2', '${data.B_ASK_KEYNO}')">
       <td class="tdR">&nbsp;${data.B_ASK_TITLE }</td>
     </tr>
   </c:forEach>
   <c:if test="${fn:length(resultList2) eq 0 }">
    <tr>
      <td colspan="2" style="text-align:center;"><b>목록이 없습니다.</b></td>
    </tr>
   </c:if>
</table>

</div> --%>

<div class="home_miribogi">
  <div class="home_barogagi_box" onclick="detailLink('overview', 'ov1')">
    <img src="/resources/css/images/common/home_1.gif" alt="바로가기1"/>  
  </div>
  <div class="home_barogagi_box" onclick="detailLink('product', 'pd1')">
    <img src="/resources/css/images/common/home_2.gif" alt="바로가기1"/>  
  </div>
  <div class="home_barogagi_box" onclick="detailLink('overview', 'ov2')">
    <img src="/resources/css/images/common/home_3.gif" alt="바로가기1"/>  
  </div>
  <div class="home_barogagi_box" onclick="detailLink('overview', 'ov4')">
    <img src="/resources/css/images/common/home_4.gif" alt="바로가기1"/>  
  </div>
</div>
