<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"  />
<title>알수없는 오류가 발생 했습니다.</title>
<script type="text/javascript">
</script>   
</head>    
<body>	
<br /><br />
<center>
<table width="450">
	<tr>
		<td align="left">
		<h2>죄송합니다.<br /><br />
		요청하신 페이지를 찾을수 없습니다.
		</h2>
		<div class="none"><br /></div>
		<p>방문하시려는 페이지의 주소가 잘못 입력되었거나,<br />페이지의 주소가 변경 혹은 삭제되어 요청하신 페이지를 찾을 수 없습니다.</p>
		    <p>입력하신 주소가 정확한지 다시 한번 확인해 주시기 바랍니다.</p>
		    <p>관련 문의사항은 고객센터에 알려주시면 친절하게 안내해 드리겠습니다.</p>
		    <p>감사합니다.</p>
		</td>
	</tr>
</table>   
</center>
</body>
</html>