<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

    <div class="contents_title">
      <div class="page_title">일루문&nbsp;<span>ILLUMOON</span></div>
      <div class="location">home / 회사소개 / 일루문</div>
      <div class=" border3 clear"></div>
    </div>
    
    <div class="content">
      
      <div class="con_top"><img src="/resources/css/images/common/deco1.png" style="margin-right:5px;"><span>기업정보</span></div>
    
      <table class="data_list">
        <colgroup>
          <col style="width: 30%;" />
          <col style="width: 70%;" />
        </colgroup>

        <tbody>
          <tr><td class="tdL">회사명</td> <td class="tdR">일루문(ILLUMOON)</td></tr>
          <tr><td class="tdL">대표자</td> <td class="tdR">문정훈</td></tr>
          <tr><td class="tdL">사업자등록번호</td> <td class="tdR">208-27-55687</td></tr>
          <tr><td class="tdL">등록분야</td> <td class="tdR">제조(LED 조명 Power 및 System 개발 및 제조)</td></tr>
          <tr><td class="tdL">주소</td> <td class="tdR">광주광역시 남구 봉선중앙로 8, 102동 801호(봉선동, 쌍용스윗닷홈)</td></tr>
          <tr><td class="tdL">연락처</td> <td class="tdR">TEL: 070-4001-0979 // FAX: 050-4377-0979</td></tr>
          <tr><td class="tdL">회사설립 연도</td> <td class="tdR">2016</td></tr>


        </tbody>
      </table>
      <tfoot align="center">
      </tfoot>

      </table>

    </div>