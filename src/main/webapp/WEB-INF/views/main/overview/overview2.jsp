<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="contents_title">
      <div class="page_title">CEO인사말&nbsp;<span>WELCOME</span></div>
      <div class="location">home / 회사소개 / CEO인사말</div>
      <div class=" border3 clear"></div>
    </div>
    
    <div class="content">
      
    <div class="content_sub_2divWrap">
	    <div class="content_sub_div1">
		    <img src="/resources/css/images/common/ceo_img.gif" alt="회사정보" />
	    </div>
	    <div class="content_sub_div2">
				<h3 style="margin:0;">당사 홈페이지를 찾아 주셔서 진심으로 감사드립니다.</h3><br/>
				일루문은 축적된 독보적 기술로 우수한 가성비의<br/>
				고효율 LED 조명을 제작하는 전문 기업입니다.<br/><br/>
				가족이 쓸 조명을 제조하는 마음으로<br/>
				항상 안전하고 효율적인 빛을 제공하도록 노력하겠습니다.<br/><br/>
				감사합니다.
				<br/><br/><br/>
				<b style="color:#333; float:right;">CEO 문정훈 올림</b>
	    </div>
    </div>

      </table>

    </div>