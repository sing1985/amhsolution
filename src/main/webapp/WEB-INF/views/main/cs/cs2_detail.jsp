<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>
<style>
.style1 {color: #D1862C}
</style>

<script>
boardType = '/cs2' // 게시판 url 분기처리
</script>

<div class="contents_title">
      <div class="page_title">공지사항&nbsp;<span>NOTICE</span></div>
      <div class="location">home / 회사소개 / 공지사항</div>
      <div class=" border3 clear"></div>
    </div>
    
    <div class="content">
      <form id="registForm">
      <input type="hidden" name="B_KEYNO" value="${resultDetail.B_ASK_KEYNO }" readonly/>
      <input type="hidden" name="B_PWD" value="${resultDetail.B_ASK_PASSWORD }" readonly/>
      <table width="95%" border="0" cellspacing="0" cellpadding="0">
      <tbody>
      <tr>
        <td align="right" height="25">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody><tr align="right">
              <td width="70%">
                <div align="left">                
                </div>
                <div align="left">
                  <font color="#8e9aa9"><b>Hit:</b></font> 
                  <font style="FONT-SIZE: 8pt; FONT-FAMILY: Tahoma" color="#a0a0a0"><b> 386</b></font> 
                    &nbsp;&nbsp;&nbsp; 
                  <font color="#8e9aa9"></font>
                  <font color="#8e9aa9"><b>Date:</b></font> 
                  <font style="FONT-SIZE: 8pt; FONT-FAMILY: Tahoma" color="#a0a0a0"><b> ${resultDetail.B_ASK_REGDATE }</b></font>
                </div>
              </td>
            </tr>
          </tbody></table>        
        </td>
      </tr>
      <tr>
        <td background="/resources/css/images/dt_img/titlebar_bg.gif" height="18"></td>
      </tr>
      <tr>
        <td height="1" background="/resources/css/images/dt_img/line_dot.gif"></td>
      </tr>
      <tr>
        <td height="25">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="25">
            <tbody><tr>
              <td width="20%" bgcolor="#FDEBD5">
                <div align="right">
                  <font class="style3 style1"><code><strong>Title</strong></code></font>
                  <font color="#000033"></font>
                  <b><font color="#000033">&nbsp;</font></b>                </div>
              </td>
<%--               <td width="88%">&nbsp; <b>${resultDetail.B_ASK_TITLE }</b></td> --%>
              <td width="88%">&nbsp; 
                 <input class="input" style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-LEFT: gray 1px solid; 
                 COLOR: #adadad; BORDER-BOTTOM: gray 1px solid; BACKGROUND-COLOR: #ffffff" maxlength="20" name="B_ASK_TITLE" type="text"
                 value="${resultDetail.B_ASK_TITLE }" readonly>
              </td>
            </tr>
          </tbody></table>
        </td>
      </tr>
      <tr>
        <td height="1" background="/resources/css/images/dt_img/line_dot.gif"></td>
      </tr>
      <tr>
        <td height="21" valign="top">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="35">
            <tbody><tr>
              <td valign="top" align="left">
                <div align="right">
                  <b></b>               
                </div> <br>
                <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                  <tbody><tr>
                    <td align="left">
                      <textarea readonly class="input" style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-LEFT: gray 1px solid; 
		                 COLOR: #adadad; BORDER-BOTTOM: gray 1px solid; BACKGROUND-COLOR: #ffffff" maxlength="150" name="B_ASK_CONTENTS"
		                 rows="10" cols="80">${resultDetail.B_ASK_CONTENTS }</textarea>
                    </td>
                  </tr>
                </tbody></table> <br> <br>
              </td>
            </tr>
            <tr>
              <td valign="middle" align="center" height="1" background="/resources/css/images/dt_img/line_dot.gif"></td>
            </tr>
          </tbody></table>
        </td>
      </tr>
      
      <tr>
        <td height="5"></td>
      </tr>
      <tr>
        <td height="5" background="/resources/css/images/dt_img/titlebar_bg2.gif"></td>
      </tr>
      <tr>
        <td height="30" valign="middle">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
	            <tr>
	              <td align="right">
                  <a href="JavaScript:doModify('${map.currentPageNo }')" class="writeBtnBox" style="display:none;">
                   <img src="/resources/css/images/dt_img/write.gif" width="46" height="15" border="0">
                  </a>
                </td>
	              <td width="50" align="right">
	                <a href="JavaScript:goModifyForm('.input');showAndHide('.writeBtnBox')">
	                 <img src="/resources/css/images/dt_img/modify.gif" width="46" height="15" border="0">
	                </a>
	              </td>
	              <td width="50" align="right">
                  <a href="javascript:linkPage('${map.currentPageNo }');"><img src="/resources/css/images/dt_img/list.gif" width="46" height="15" border="0"></a>
	              </td>
	            </tr>
            </tbody>
          </table>
        </td>
      </tr>

      </tbody>
    </table>
   </form>

   </div>