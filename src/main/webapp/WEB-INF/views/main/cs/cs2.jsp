<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>

<script>
boardType = '/cs2' // 게시판 url 분기처리
</script>

<div class="contents_title">
      <div class="page_title">문의게시판&nbsp;<span>NOTICE</span></div>
      <div class="location">home / 회사소개 / 문의게시판</div>
      <div class=" border3 clear"></div>
    </div>
    
    <div class="content">
      
    <table width="95%" border="0" cellspacing="0" cellpadding="0">
    
      <tbody>
        <tr>
          <td background="/resources/css/images/dt_img/titlebar_bg.gif" height="18">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
               <tr class="row_data">
                <td width="10%">
                  <div align="center"><img src="/resources/css/images/dt_img/No.gif" width="11" height="7"></div>
                </td>
                <td width="35%">
                  <div align="center"><img src="/resources/css/images/dt_img/subject.gif" width="40" height="7"></div>
                </td>
                <td width="15%"> 
                  <div align="center"><img src="/resources/css/images/dt_img/name.gif" width="23" height="7"></div>
                </td>
                <td width="40%"> 
                  <div align="center"><img src="/resources/css/images/dt_img/date.gif" width="23" height="7"></div>
                </td>
              </tr>
            </tbody></table>
          </td>
        </tr>
      
        <tr>
          <td height="18">
          
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="listTable">
	          <tbody>
	           <c:forEach items="${resultList }" var="data">
	            <tr class="row_data ${data.B_ASK_KEYNO}" onclick="detailWithPassword('${data.B_ASK_KEYNO}')">
	              <td width="10%" style="text-align:center;">${data.B_ASK_KEYNO }</td>
	              <td width="35%">${data.B_ASK_TITLE }</td>
	              <td width="15%">${data.B_ASK_NAME }</td>
	              <td width="40%" style="text-align:center;">${fn:substring(data.B_ASK_REGDATE, 0, 10) }</td>
	            </tr>
	            <tr>
	              <td colspan="5" height="1" background="/resources/css/images/dt_img/line_dot.gif"></td>
	            </tr>
	          </c:forEach>
	          <c:if test="${fn:length(resultList) eq 0 }">
	           <tr>
	             <td colspan="4" style="text-align:center;"><b>목록이 없습니다.</b></td>
	           </tr>
	          </c:if>
	         </tbody>
          </table>
        </td>
      </tr>
      <tr>
          <td height="5" background="/resources/css/images/dt_img/titlebar_bg2.gif"></td>
      </tr>
      <tr>
       <td>
	       <div style="text-align:center;">
	         <ul class="pagination">
	           <ui:pagination paginationInfo="${paginationInfo}" type="admin" jsFunction="linkPage" />
	         </ul>
	       </div>
       </td>
      </tr>
      <c:if test="${LoginInfo.UI_TYPE ne 'S' }">
        <tr>
          <td height="30" valign="middle">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
               <tr>
                <td width="6%">
                  <div align="left">
                    <a href="javascript:changeContents('/cs2/regist/view')"><img src="/resources/css/images/dt_img/write.gif" width="46" height="15" border="0"></a>
                    <input type="hidden" name="pageNumber" value="1">
                  </div>
                </td>
              </tr>
             </tbody>
            </table>
          </td>
        </tr>
      </c:if>
    </tbody>
  </table>
<%--     <c:if test="${LoginInfo.UI_TYPE eq 'S' }"> --%>
<!--      <div style="text-algin:right;"> -->
<!--        <button onclick="changeContents('/cs1/regist/view')">글쓰기</button> -->
<!--      </div> -->
<%--     </c:if> --%>
    
  </div>
    
    
    <form id="form_pageInfo">
      <input type="hidden" name="currentPageNo" value="${paginationInfo.currentPageNo }"/>
    </form>