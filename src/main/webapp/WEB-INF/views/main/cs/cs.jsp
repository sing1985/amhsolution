<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>

<c:if test="${ empty map.sml  }">
<script>
$(function(){
  $('.group').eq(0).children('div').click();
})
</script>
</c:if>

<c:if test="${ !empty map.sml  }">
<script>
var subMenuLink = "${map.sml}";
boardDetailLink = "${map.bdl}";
$(function(){
  $('.'+subMenuLink).click()
})
</script>
</c:if>