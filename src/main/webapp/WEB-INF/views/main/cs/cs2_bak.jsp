<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>

<script>
boardType = '/cs2' // 게시판 url 분기처리

$(function(){
	
	
})

function detailWithPassword(keyno){
	<c:if test="${LoginVo.UI_TYPE}">
	</c:if>
	pwd = prompt('비밀번호를 입력해주세요.');
	url = boardType + '/detail/chk/password?B_KEYNO=' + keyno + '&B_PWD=' + pwd;
	$.get(url, function(data){
// 		alert('result : ' + data);
		  if(data == 'false'){
			  alert('비밀번호가 틀렸습니다.');
		  }else{
// 			  alert("비밀번호는 : " + data);
			  detail(keyno);
		  }
    })
}

</script>

<div class="contents_title">
      <div class="page_title">공지사항&nbsp;<span>NOTICE</span></div>
      <div class="location">home / 회사소개 / 공지사항</div>
      <div class=" border3 clear"></div>
    </div>
    
    <div class="content">
      
      <div class="con_top"><img src=""><span>공지사항</span></div>
    
      <table class="data_list">
        <colgroup>
          <col style="width: 10%;" />
          <col style="width: 60%;" />
          <col style="width: 15%;" />
        </colgroup>
        <thead>
          <tr>
            <th>번호</th>
            <th>제목</th>
            <th>날짜</th>
           </tr>
        </thead>
        <tbody>
          <c:forEach items="${resultList }" var="data">
            <tr onclick="detailWithPassword('${data.B_ASK_KEYNO}')">
              <td style="text-align:center;">${data.B_ASK_KEYNO }</td>
              <td>${data.B_ASK_TITLE }</td>
              <td style="text-align:center;">${fn:substring(data.B_ASK_REGDATE, 0, 10) }</td>
            </tr>
          </c:forEach>
        </tbody>
      </table>
      <tfoot align="center">
      </tfoot>
      </table>
      
      <ul class="pagination">
        <ui:pagination paginationInfo="${paginationInfo}" type="admin" jsFunction="detailWithPassword" />
      </ul>
      
      <c:if test="${LoginInfo.UI_TYPE ne 'S' }">
        <div style="text-algin:right;">
          <button onclick="changeContents('/cs2/regist/view')">글쓰기</button>
        </div>
      </c:if>
      
    </div>
    
    <form id="form_pageInfo">
      <input type="hidden" name="currentPageNo" value="${paginationInfo.currentPageNo }"/>
    </form>