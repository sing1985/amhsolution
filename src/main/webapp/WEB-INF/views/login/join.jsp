<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>
<style>
.style1 {color: #D1862C}
</style>
<script>
boardType = '/cs2'
</script>

<div class="contents_title">
      <div class="page_title">문의게시판&nbsp;<span>ASK</span></div>
      <div class="location">home / 회사소개 / 문의게시판</div>
      <div class=" border3 clear"></div>
    </div>
    
    <div class="content">
      <form id="registForm">
      <table width="95%" border="0" cellspacing="0" cellpadding="0">
      <tbody>
      <tr>
        <td background="/resources/css/images/dt_img/titlebar_bg.gif" height="18"></td>
      </tr>
      <tr>
        <td height="1" background="/resources/css/images/dt_img/line_dot.gif"></td>
      </tr>
      
      <tr>
        <td height="30">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="25">
            <tbody><tr>
              <td width="20%" bgcolor="#FDEBD5">
                <div align="right">
                  <font class="style3 style1"><code><strong>아이디</strong></code></font>
                  <font color="#000033"></font>
                  <b><font color="#000033">&nbsp;</font></b>                </div>
              </td>
               <td width="88%">&nbsp; 
                 <input class="input" style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-LEFT: gray 1px solid; 
                 COLOR: #adadad; BORDER-BOTTOM: gray 1px solid; BACKGROUND-COLOR: #ffffff" maxlength="20" name="UI_ID" type="text">
               </td>
            </tr>
          </tbody></table>
        </td>
      </tr>
      <tr>
        <td height="1" background="/resources/css/images/dt_img/line_dot.gif"></td>
      </tr>
      
      <tr>
        <td height="30">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="25">
            <tbody><tr>
              <td width="20%" bgcolor="#FDEBD5">
                <div align="right">
                  <font class="style3 style1"><code><strong>비밀번호</strong></code></font>
                  <font color="#000033"></font>
                  <b><font color="#000033">&nbsp;</font></b>                </div>
              </td>
               <td width="88%">&nbsp; 
                 <input class="input" style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-LEFT: gray 1px solid; 
                 COLOR: #adadad; BORDER-BOTTOM: gray 1px solid; BACKGROUND-COLOR: #ffffff" maxlength="20" name="UI_PASSWORD" type="password">
               </td>
            </tr>
          </tbody></table>
        </td>
      </tr>
      <tr>
        <td height="1" background="/resources/css/images/dt_img/line_dot.gif"></td>
      </tr>
      
      
      <tr>
        <td height="30">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="25">
            <tbody><tr>
              <td width="20%" bgcolor="#FDEBD5">
                <div align="right">
                  <font class="style3 style1"><code><strong>이름</strong></code></font>
                  <font color="#000033"></font>
                  <b><font color="#000033">&nbsp;</font></b>                </div>
              </td>
               <td width="88%">&nbsp; 
                 <input class="input" style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-LEFT: gray 1px solid; 
                 COLOR: #adadad; BORDER-BOTTOM: gray 1px solid; BACKGROUND-COLOR: #ffffff" maxlength="20" name="UI_NAME" type="text">
                 
                 <input class="input" style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-LEFT: gray 1px solid; 
                 COLOR: #adadad; BORDER-BOTTOM: gray 1px solid; BACKGROUND-COLOR: #ffffff;" maxlength="300" name="UI_TYPE" value="C" type="hidden">
                 <input class="input" style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-LEFT: gray 1px solid; 
                 COLOR: #adadad; BORDER-BOTTOM: gray 1px solid; BACKGROUND-COLOR: #ffffff;" maxlength="300" name="UI_USEYN" value="Y" type="hidden">
                 
               </td>
            </tr>
          </tbody></table>
        </td>
      </tr>
      <tr>
        <td height="1" background="/resources/css/images/dt_img/line_dot.gif"></td>
      </tr>
      
      <tr>
	       <td height="30">
	         <table width="100%" border="0" cellspacing="0" cellpadding="0" height="30">
	           <tbody><tr>
	             <td width="20%" bgcolor="#FDEBD5">
	               <div align="right">
	                 <b><font color="#D1862C">연락처</font>
	                    <font color="#000033">&nbsp;</font></b>
	               </div>
	             </td>
	             <td width="88%">&nbsp; 
	               <input class="input" style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-LEFT: gray 1px solid; 
	               COLOR: #adadad; BORDER-BOTTOM: gray 1px solid; BACKGROUND-COLOR: #ffffff" maxlength="20" name="UI_TEL" type="text">
	             </td>
	           </tr>
	         </tbody></table>
	       </td>
	     </tr>
	     <tr>
        <td height="1" background="/resources/css/images/dt_img/line_dot.gif"></td>
      </tr>
      
      
      <tr>
        <td height="30">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="30">
            <tbody><tr>
              <td width="20%" bgcolor="#FDEBD5">
                <div align="right">
                  <b><font color="#D1862C">이메일</font>
                     <font color="#000033">&nbsp;</font></b>
                 </div>
               </td>
               <td width="88%">&nbsp; 
                 <input class="input" style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-LEFT: gray 1px solid; 
                 COLOR: #adadad; BORDER-BOTTOM: gray 1px solid; BACKGROUND-COLOR: #ffffff" maxlength="20" name="UI_EMAIL" type="text">
               </td>
             </tr>
           </tbody>
         </table>
         </td>
       </tr>
       <tr>
        <td height="1" background="/resources/css/images/dt_img/line_dot.gif"></td>
      </tr>
      
      <tr>
        <td height="5"></td>
      </tr>
      <tr>
        <td height="5" background="/resources/css/images/dt_img/titlebar_bg2.gif"></td>
      </tr>
      <tr>
        <td height="30" valign="middle">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          
            <tbody>
            
            <tr>
        <td height="30" valign="middle">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody><tr>
<!--               <td width="50"> -->
<%--                 <a href="javascript:linkPage('${map.currentPageNo }')"> --%>
<!--                 <img src="/resources/css/images/dt_img/list.gif" width="46" height="15" border="0"></a> -->
<!--               </td> -->
              <td>
                <div align="right">
                  <a href="JavaScript:doJoin()">
                  <img src="/resources/css/images/dt_img/continue.gif" width="55" border="0"></a>
                </div>
              </td>
            </tr>
          </tbody></table>
        </td>
      </tr>
            
          </tbody></table>
        </td>
      </tr>

    </tbody></table>
      
    </form>
      

    </div>