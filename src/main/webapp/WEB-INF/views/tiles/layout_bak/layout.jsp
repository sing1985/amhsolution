<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<tiles:insertAttribute name="head" /> 
		<tiles:insertAttribute name="script" />
		<tiles:insertAttribute name="css" />
		<title>Sample Project</title>
	</head>   
<body>         
	<tiles:insertAttribute name="header" />
<div id="container">	
	<div id="leftContaner">
		<tiles:insertAttribute name="leftmenu" />
	</div>
    <tiles:insertAttribute name="contents" /> 
    <tiles:insertAttribute name="footer" />  
</div>
</body>
</html>