<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>
<div id="LeftMenu"> 
 	<ul id="navi">
	        <li class="group">
	            <div class="title" >인사 기본정보</div>
	            <ul class="sub">
	                <li><a href="#">인사마스터관리</a></li>
	                <li><a href="#">직원전체조회</a></li>
	                <li><a href="#">자격증현황조회</a></li>
	            </ul>
	        </li>
	        <li class="group">
	            <div class="title" >신원보증관리</div>
	            <ul class="sub">
	                <li><a href="#">신원보증사항등록</a></li>
	                <li><a href="#">신원보증조회</a></li>
	            </ul>
	        </li>
	        <li class="group">
	            <div class="title" >인사발령관리</div>
	            <ul class="sub">
	                <li><a href="#">인사발령내역관리</a></li>
	                <li><a href="#">인사발령내역조회</a></li>
	                <li><a href="#">퇴직자조회</a></li>
	                <li><a href="#">시험일정, 대상자등록</a></li>
	                <li><a href="#">시험점수등록</a></li>
	                <li><a href="#">승급대상자관리</a></li>
	                <li><a href="#">승진대상자조회</a></li>
	                <li><a href="#">승진평균소요연수조회</a></li>
	                <li><a href="#">징계자리스트조회</a></li>
	            </ul>
	        </li>
	        <li class="group">
	            <div class="title" >표창, 교육관리</div>
	            <ul class="sub">
	                <li><a href="#">표창, 서훈사항등록</a></li>
	                <li><a href="#">표창, 서훈조회</a></li>
	                <li><a href="#">교육, 연수일정관리</a></li>
	                <li><a href="#">교육, 연수신청</a></li>
	                <li><a href="#">교육, 연수평가</a></li>
	                <li><a href="#">해외연수관리</a></li>
	            </ul>
	        </li>
	        <li class="group">
	            <div class="title" >휴가,출장,근태관리</div>
	            <ul class="sub">
	                <li><a href="#">휴가관리</a></li>
	                <li><a href="#">휴가내역조회</a></li>
	                <li><a href="#">출장관리</a></li>
	                <li><a href="#">출장내역조회</a></li>
	                <li><a href="#">당직, 숙직관리</a></li>
	                <li><a href="#">당직, 숙직내역조회</a></li>
	                <li><a href="#">근태관리</a></li>
	                <li><a href="#">근태내역조회</a></li>
	            </ul>
	        </li>
	        <li class="group">
	            <div class="title" >기타</div>
	            <ul class="sub">
	                <li><a href="#">지부별정원관리</a></li>
	                <li><a href="#">지부별인원조회</a></li>
	                <li><a href="#">양식출력</a></li>
	            </ul>
	        </li>
    </ul>
    
    
    
    
</div> 
