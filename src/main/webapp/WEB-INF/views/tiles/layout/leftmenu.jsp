<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>

<style>
#leftContaner { background-color:#FFF; font-weight:bold;}
#LeftMenu div { color:#7c7d7d;}

#LeftMenu ul li { margin-right:5px; }
#LeftMenu ul li div { text-align:right; padding: 0 10px; }
.group {border:solid 2px #FFF;}
.selectedMenu {
  font-family: fantasy;
  background: linear-gradient(to left, #e77903, #f79723);
  background: #70bf32;
  background: -webkit-linear-gradient(to left, #e77903, #f79723);
  background:    -moz-linear-gradient(to left, #e77903, #f79723);
  background:     -ms-linear-gradient(to left, #e77903, #f79723);
  background:      -o-linear-gradient(to left, #e77903, #f79723);
  background:         linear-gradient(to left, #e77903, #f79723);
  border: solid 2px #FFF;
  box-shadow: 1px 1px 3px #888;
  text-shadow: 1px 1px 1px #888;
}
.selectedMenu div { color:#FFF !important;}

#pageName_Box { padding:15px; text-align:right; margin-bottom:20px;}
#pageName_Box .pageName_big {font-size:32px;}
#pageName_Box .pageName_small {font-size:12px; margin-top:3px;}
</style>


<div id="pageName_Box">
  <div class="pageName_big">${PAGE_NAME_BIG }</div>
  <div class="pageName_small">${PAGE_NAME_SMALL }</div>
  <div></div>
</div>
<c:if test="${ !empty PAGE_NAME_BIG }">
<div id="LeftMenu"> 
 	<ul id="navi">
 	  <c:if test="${PAGE_NAME_BIG eq 'OVERVIEW' }">
		  <li class="group selectedMenu">
		    <div class="title ov1" onclick=" changeContents('/overview1.do')">일루문</div>
		  </li>
		  <li class="group">
		    <div class="title ov2" onclick=" changeContents('/overview2.do')">CEO인사말</div>
		  </li>
		  <li class="group">
		    <div class="title ov3" onclick=" changeContents('/overview3.do')">주요연혁</div>
		   </li>
		   <li class="group">
		     <div class="title ov4" onclick=" changeContents('/overview4.do')">찾아오시는길</div>
		   </li>
 	  </c:if>
 	  
 	  <c:if test="${PAGE_NAME_BIG eq 'PRODUCT' }">
      <li class="group selectedMenu">
        <div class="title pd1" onclick=" changeContents('/product1.do')">LED</div>
      </li>
      <!-- <li class="group PD2">
        <div class="title" onclick=" changeContents('/product2.do')">삼파장</div>
      </li>
      <li class="group PD3">
        <div class="title" onclick=" changeContents('/product3.do')">반짝반짝</div>
       </li> -->
    </c:if>
    
    <c:if test="${PAGE_NAME_BIG eq 'CS' }">
      <li class="group selectedMenu">
        <div class="title cs1" onclick=" changeContents('/cs1.do')">공지사항</div>
      </li>
      <li class="group ">
        <div class="title cs2" onclick=" changeContents('/cs2.do')">문의게시판</div>
      </li>
    </c:if>
   </ul>
</div> 
</c:if>

<div id="leftmenu_banner">
  <div onclick="detailLink('cs', 'cs2')">
    <img src="/resources/css/images/common/left_banner1.gif" alt="바로가기1"/>  
  </div>
  <div onclick="detailLink('cs', 'cs2')">
    <img src="/resources/css/images/common/left_banner2.gif" alt="바로가기2"/>  
  </div>
</div>
