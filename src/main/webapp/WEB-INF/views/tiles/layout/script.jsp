<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" 		src="/resources/js/jquery-1.8.3.js"></script>
<script type="text/javascript" 		src="/resources/js/submenu.js"></script>
<script type="text/javascript" 		src="/resources/js/font.js"></script>
<script type="text/javascript" 		src="/resources/js/jquery-ui.js"></script>
<script type="text/javascript" 		src="/resources/js/function.js"></script>

<!-- daum지도 API  -->
<script charset="UTF-8" class="daum_roughmap_loader_script" src="http://dmaps.daum.net/map_js_init/roughmapLoader.js"></script>


<script>

var boardType = ''; //메뉴타입
var pwd = ''; //cs2 패스워드
var boardDetailLink = ''; //게시판상세링크

$(function(){
  $('.group').on('click', function(){
    $('.group').removeClass('selectedMenu');
    $(this).addClass('selectedMenu');
  });
})


/* 메인 - 서브메뉴
* 메인 - 서브메뉴 - 상세게시판 내용 링크
 */
function detailLink( url, sml, bdl){
  var goUrl = url + '.do?sml=' + sml;
//  alert("bdl:"+bdl)
  if( bdl !== undefined ){
    goUrl += '&bdl=' + bdl;
  }
  location.href = goUrl;
}

/* 컨텐츠 변경 */
function changeContents(url){
	url += '.do?' + $('#form_pageInfo').serialize();
  $.get(url, function(data){
      $('#Contents').html(data).queue(function(){
//          alert("큐 : 클릭 - "+boardDetailLink);
          if(boardDetailLink != ""){
            $('.'+boardDetailLink).click();
          }
        });;
  })
}

function linkPage(num){
//  alert(boardType)
  var url = boardType + ".do?currentPageNo="+num;
//  pageIndex = num;
  $.get(url, function(data){
      $('#Contents').html(data);
  })
}

function detail(keyno){
  //alert(boardType)
  var url = boardType + "/detail.do?B_KEYNO="+keyno + "&B_PWD="+pwd;
//  alert($('#form_pageInfo').serialize())
  url += '&' + $('#form_pageInfo').serialize();
    $.get(url, function(data){
        $('#Contents').html(data);
    })
}

function doWrite(currentPageNo){
   var url = boardType + "/regist/action.do?currentPageNo="+currentPageNo;
   url += '&' + $('#registForm').serialize();
//    alert(url);
    $.get(url, function(data){
        $('#Contents').html(data);
    })
}

function doModify(currentPageNo){
	   var url = boardType + "/modify/action.do?currentPageNo="+currentPageNo;
	   url += '&' + $('#registForm').serialize();
//	    alert(url);
	    $.get(url, function(data){
	        $('#Contents').html(data);
	    })
	}

var formReadOnly = true;
function goModifyForm($target){
	if( $($target).eq(0).prop('readonly') ){
			$($target).prop('readonly',false);
			formReadOnly = false;
		}else{
			$($target).prop('readonly',true);
			formReadOnly = true;
		}
}

function showAndHide($target){
	if($($target).css("display") == "none"){
	      $($target).show();
	  } else {
	      $($target).hide();
	  }
}

function detailWithPassword(keyno){
    pwd = prompt('비밀번호를 입력해주세요.');
    if( pwd == null ) return false;
    
    url = boardType + '/detail/chk/password.do?B_KEYNO=' + keyno + '&B_PWD=' + pwd;
    $.get(url, function(data){
//      alert('result : ' + data);
        if(data == 'false'){
          alert('비밀번호가 틀렸습니다.');
        }else{
//          alert("비밀번호는 : " + data);
          detail(keyno);
        }
      })
  }
  

function doJoin(){
   var url = "/join/action.do";
  $('#registForm').attr('action',url);
  $('#registForm').submit();
}
  
</script>


