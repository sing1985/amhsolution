<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<tiles:insertAttribute name="head" /> 
		<tiles:insertAttribute name="script" />
		<tiles:insertAttribute name="css" />
		<title>일루문 LED</title>
	</head>   
<body>
	<tiles:insertAttribute name="header" />
<div id="bodyWrapper">
	<div id="container">	
		<div id="leftContaner">
			<tiles:insertAttribute name="leftmenu" />
		</div>
		<div id="Contents">
	    <tiles:insertAttribute name="contents" /> 
		</div>
	    <tiles:insertAttribute name="footer" />  
	</div>
</div>         
</body>
</html>