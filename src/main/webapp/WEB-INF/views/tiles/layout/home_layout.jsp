<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<tiles:insertAttribute name="head" /> 
		<tiles:insertAttribute name="script" />
		<tiles:insertAttribute name="css" />
		<title>일루문 LED 홈페이지에 오신걸 환영합니다.</title>
		<c:if test="${PAGE_NAME_BIG eq 'HOME' }">
		</c:if>
	</head>   
<body>
	<tiles:insertAttribute name="header" />
<div id="bodyWrapper">
	<div id="container">	
		<div id="Contents">
	    <tiles:insertAttribute name="contents" /> 
		</div>
	    <tiles:insertAttribute name="footer" />  
	</div>
</div>         
</body>
</html>