<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>
<link href="/resources/css/css.css" rel="stylesheet" />
<link href="/resources/css/jquery-ui.css" rel="stylesheet" />
<link href="/resources/css/submenu.css" rel="stylesheet" />
 
 
<style>
textarea {resize: none;}
pre {white-space: initial;}
.clear{clear: both;}
.contents_title{margin-top: 10px;}
.contents_title .page_title{float: left; font-size: 30px; font-weight: bold;}
.contents_title .location{float: right;}
.page_title span{font-size: 15px; color: gray;}
.border2{border-bottom: 2px solid gray; padding-top: 10px;}
.border3{border-bottom: 3px solid gray; padding-top: 10px;}


.content_sub_2divWrap {}
.content_sub_div1 { width:40%; float:left;}
.content_sub_div1 img {width:100%;}
.content_sub_div2 { width:50%; float:left; line-height:1.3; padding:5%; vertical-align:middle; font-size:15px; color:#5f5f5f;}

.content{margin-top: 20px; padding: 15px;}
.con_top{margin-bottom: 5px;}
.con_top span{font-size: 15px; font-weight: bord;}
.data_list{border-top: 3px solid gray; width: 100%; border-spacing: 0px 0px;}
.data_list td{padding: 10px; border-bottom: 1px solid #dedede; font-size: 13px;  }
.data_list .tdL{text-align: right; padding-right: 15px; border-right: 1px solid gray; background-color: #F5F5F5;}
.data_list .tdR{text-align: left; padding-left: 15px;}

input.input[readonly], textarea.input[readonly] {color:#222 !important; border-color:#FFF !important;}

.listTable tr.row_data td { height:30px; text-align:center; cursor:pointer;}
.listTable tr.row_data:HOVER {background-color:#ffd6a4;} 

.right_deco {padding:0 10px; margin-right:15px; border-right:solid 2px #ff9360;}
.history_month {}

.pagination { text-align:center; font-size:15px;}

.footer{padding: 10px 0px;}
.footer div{margin-top: 5px;}
.footer_dsc{width: 80%; float:left; margin-top: 15px; margin-left: 15px;}
.f_add{float: left; width:50%;}
.f_tel{float: left; width:50%;}
.f_name{clear: both;}

.footer .logo{width:15%; float:right; margin-top: 15px;}
.footer .logo img{width:100%;}
.footer span{font-size: 12px; color:#757575; margin-right: 5px; font-weight: bold;}
.footer{font-size: 10px; color:#9E9E9E;}
.footer .f_text{font-size: 8px; color:#BDBDBD; margin-top: 8px;}


</style>











<!-- 페이징처리 - 스마트어드민 레이아웃 커스터마이징 -->
<style>
    
.pagination {
  display: inline-block;
  padding-left: 0;
  margin: 18px 0;
  border-radius: 2px
}

.pagination>li {
  display: inline
}

.pagination>li>a,.pagination>li>span {
  position: relative;
  float: left;
  padding: 3px 9px;
  line-height: 1.42857143;
  text-decoration: none;
  color: #f79723;
  background-color: #fff;
  border: 1px solid #f79723;
  margin-left: -1px
}

.pagination>li:first-child>a,.pagination>li:first-child>span {
  margin-left: 0;
  border-bottom-left-radius: 2px;
  border-top-left-radius: 2px
}

.pagination>li:last-child>a,.pagination>li:last-child>span {
  border-bottom-right-radius: 2px;
  border-top-right-radius: 2px
}

.pagination>li>a:focus,.pagination>li>a:hover,.pagination>li>span:focus,.pagination>li>span:hover
  {
  color: #FFF;
  background-color: #f79723;
  border-color: #f79723
}

.pagination>.active>a,.pagination>.active>a:focus,.pagination>.active>a:hover,.pagination>.active>span,.pagination>.active>span:focus,.pagination>.active>span:hover
  {
  z-index: 2;
  color: #fff;
  background-color: #f79723;
  border-color: #f79723;
  cursor: default
}

.pagination>.disabled>a,.pagination>.disabled>a:focus,.pagination>.disabled>a:hover,.pagination>.disabled>span,.pagination>.disabled>span:focus,.pagination>.disabled>span:hover
  {
  color: #999;
  background-color: #fff;
  border-color: #ddd;
  cursor: not-allowed
}

.pagination-lg>li>a,.pagination-lg>li>span {
  padding: 10px 16px;
  font-size: 17px
}

.pagination-lg>li:first-child>a,.pagination-lg>li:first-child>span {
  border-bottom-left-radius: 3px;
  border-top-left-radius: 3px
}

.pagination-lg>li:last-child>a,.pagination-lg>li:last-child>span {
  border-bottom-right-radius: 3px;
  border-top-right-radius: 3px
}

.pagination-sm>li>a,.pagination-sm>li>span {
  padding: 5px 10px;
  font-size: 12px
}

.pagination-sm>li:first-child>a,.pagination-sm>li:first-child>span {
  border-bottom-left-radius: 2px;
  border-top-left-radius: 2px
}

.pagination-sm>li:last-child>a,.pagination-sm>li:last-child>span {
  border-bottom-right-radius: 2px;
  border-top-right-radius: 2px
}

.pager {
  padding-left: 0;
  margin: 18px 0;
  list-style: none;
  text-align: center
}

.pager li {
  display: inline
}

.pager li>a,.pager li>span {
  display: inline-block;
  padding: 5px 14px;
  background-color: #fff;
  border: 1px solid #ddd;
  border-radius: 15px
}

.pager li>a:focus,.pager li>a:hover {
  text-decoration: none;
  background-color: #eee
}

.pager .next>a,.pager .next>span {
  float: right
}

.pager .previous>a,.pager .previous>span {
  float: left
}

.pager .disabled>a,.pager .disabled>a:focus,.pager .disabled>a:hover,.pager .disabled>span
  {
  color: #999;
  background-color: #fff;
  cursor: not-allowed
}
</style>