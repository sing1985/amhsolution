<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib/taglib.jsp" %>

<script>
var msg = "${msg}";
if( msg != "" ){
	alert(msg);
}
</script>

<div id="header_bar">
  <div class="bar_box">
    <c:if test="${LoginInfo eq null }">
	    <a href="/join.do">
	     <span class="bar_btn" >JOIN</span>
	    </a>
	    <a href="/login/page.do">
	     <span class="bar_btn">LOGIN</span>
      </a>
    </c:if>
    <c:if test="${LoginInfo ne null }">
      <c:if test="${LoginInfo.UI_TYPE eq 'S' }">[관리자]&nbsp;</c:if>
      <span style="margin-right:20px;">${LoginInfo.UI_NAME }님 반갑습니다.</span>
      <a href="/logout">
        <span class="bar_btn">LOGOUT</span>
      </a>
    </c:if>
  </div>
</div>
<div id="header">
		<div id="Logo" onclick="HomeViewMove()">
		  <div class="Logo_wrapp">
	      <img src="/resources/css/images/common/logo_color.png" align="middle"></a>
		  </div>
    </div>
		<div id="header_menu">
			<a href="/overview.do">회사소개</a>
			<a href="/product.do">제품현황</a>
			<a href="/cs.do">고객센터</a> 
		</div> 
</div>
<!-- <div id="header_bottom" class="vertical stripes"> -->
<div id="header_bottom">
<div class="header_gradient">
  <div class="header_bottom_page_name">${PAGE_NAME_BIG}</div>
  <div class="header_bottom_comment">
    CUSTOMERS TO MAKE THE FUTURE BRIGHTER COMPANY<br/>
    <span><b>ILLUMOON</b></span>
  </div>
</div>
</div>
<div class="clear"></div>