<%@page import="MVC.BoardBean"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html> 
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0, minimum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script	src="../framework/jquery.mobile-1.2.0.min.js"></script>
<link rel="stylesheet"	href="../framework/jquery.mobile-1.2.0.min.css" />
<!-- <link rel="shortcut icon" href="img3/icon.png"> -->
<script src="../framework/my.js"></script>

<%
	BoardBean board = (BoardBean)request.getAttribute("boardBean");
	String mobile = (String)request.getAttribute("mobile");
	session.setAttribute("mobile", mobile);
	

	
%>

<script type="text/javascript">
function home(){
	location.href ="M_main.do?mobile=<%=mobile%>";
}

function memberManage(){
	location.href = "mMemberManage.do";
}

function estimate(){
	location.href="partList.do?mobile=<%=mobile%>";
}

function calander(){
	location.href="mCalander.do";
}

function mSMS(){
	location.href="mSms.do";
}


</script>
 

<title>다텍 모바일 메인 페이지</title>
</head>
<body>
<div data-role="page">
	<div data-role="header"> 
		<h3> da-tec </h3>
		 <a href="#" data-rel="back" data-direction="reverse"  data-icon="back">이전</a>
		<a href="#" data-role="button" onclick="home()" data-direction="reverse"  data-icon="home">홈</a>
	</div>

<div style="padding-top:10%;padding-bottom:20%;text-align:center;">
<table width="100%">
	<tr> 
		<th>    
			<%=board.getId()%> 님 환영합니다. 
		</th>
	</tr>
</table>
</div >


<table width="100%">
	<tr>
		<td align="center" width="50%">   
			<a href="#" onclick="memberManage()" data-role="button"  data-icon="check" data-iconpos="top">고객관리 </a>
		</td>
		<td align="center" width="50%">   
			<a href="#" onclick="estimate();" data-role="button"  data-icon="check" data-iconpos="top">견적서작성</a>
		</td>
	</tr>
	
	<tr>
		<td align="center" width="50%">   
			<a href="#" onclick="mSMS();" data-role="button"  data-icon="search" data-iconpos="top">SMS</a>
		</td> 
		<td align="center" width="50%">   
			<a href="#" onclick="calander();" data-role="button"  data-icon="alert" data-iconpos="top">일정</a>
		</td>
		<!-- 
			<td align="center">   
				<a href="#" onclick="logout()" data-role="button" data-inline="true" data-icon="delete" data-iconpos="top">로그아웃</a>
			</td>
		-->
	</tr>
</table>



<div data-role="footer" data-position="fixed">
	<div data-role="navbar">
		<ul>
			<li><a href="#" onclick="memberManage()"   data-icon="grid">고객관리</a></li>
			<li><a href="#" onclick="estimate();" data-icon="star">견적서</a></li>
			<li><a href="#" onclick="mSMS()" data-icon="gear">SMS</a></li>
			<li><a href="#" onclick="calander()" data-icon="grid">일정</a></li>
		</ul>
	
	</div>
</div>
</div>
</body>
</html>