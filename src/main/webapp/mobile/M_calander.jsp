<%@page import="MVC.mobile.M_calanderBeans"%>
<%@ page import="java.util.*,java.text.SimpleDateFormat"%>
<%@page import="MVC.BoardBean"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0, minimum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script	src="../framework/jquery.mobile-1.2.0.min.js"></script>
<link rel="stylesheet"	href="../framework/jquery.mobile-1.2.0.min.css" />
<!-- <link rel="shortcut icon" href="img3/icon.png"> -->
<script src="../framework/my.js"></script>

<%
	BoardBean board = (BoardBean)request.getAttribute("boardBean");
	String mobile = (String)request.getAttribute("mobile");
%>

<script type="text/javascript">
function home(){
	location.href = "M_main.do?mobile=<%=mobile%>";
}

function memberManage(){
	location.href = "mMemberManage.do";
}

function estimate(){
	location.href="partList.do?mobile=<%=mobile%>";
}

function calander(){
	location.href="mCalander.do";
}

function mSMS(){
	location.href="mSms.do";
}
/**
 * 금월조회
 */
function ThisMonth(){
	location.href="mCalander.do";
}

function fnregist(date){
	
	location.href="mCalanderMM.do?Date="+date;
}

</script>
 

<title>다텍 모바일 메인 페이지</title>
</head>
<body>
<div data-role="page">
	<div data-role="header"> 
		<h3> da-tec </h3>
		 <a href="#" data-rel="back" data-direction="reverse"  data-icon="back">이전</a>
		<a href="#" data-role="button" onclick="home()" data-direction="reverse"  data-icon="home">홈</a>
	</div>

<%
java.util.Calendar cal = java.util.Calendar.getInstance();
String strYear = request.getParameter("year");
String strMonth = request.getParameter("month");
int year = cal.get(java.util.Calendar.YEAR);
int month = cal.get(java.util.Calendar.MONTH);
int date = cal.get(java.util.Calendar.DATE);
if(strYear != null)
{
  year = Integer.parseInt(strYear);
  month = Integer.parseInt(strMonth);
}else{
}
//년도/월 셋팅
cal.set(year, month, 1);
int startDay = cal.getMinimum(java.util.Calendar.DATE);
int endDay = cal.getActualMaximum(java.util.Calendar.DAY_OF_MONTH);
int start = cal.get(java.util.Calendar.DAY_OF_WEEK);
int newLine = 0;


ArrayList<M_calanderBeans> data = (ArrayList<M_calanderBeans>)request.getAttribute("DATAS");


%>

<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" >
	<tr> 
		<td align="center">
			<%if(month > -1 && month <1 ){ %>
			<a href="mCalander.do?year=<%=year-1%>&amp;month=<%=11%>" data-role="button"  data-inline="true" data-icon="arrow-l" data-iconpos="left">이전달</a>
				<%}%>
				<%if(month > 0 ){ %> 
			<a href="mCalander.do?year=<%=year%>&amp;month=<%=month-1%>" data-role="button"  data-inline="true" data-icon="arrow-l" data-iconpos="left">이전달</a>
			<%}%> 
		</td>
		<td align="center">
			<h2><%=year%>/<%=month+1%></h2>
		</td> 
		<td align="center">
			<%if(month < 12 && month >10 ){ %>
			<a href="mCalander.do?year=<%=year+1%>&amp;month=<%=0%>" data-role="button"  data-inline="true" data-icon="arrow-r" data-iconpos="right">다음달</a> 
	           <%}%>
	           <%if(month < 11 ){ %>
			<a href="mCalander.do?year=<%=year%>&amp;month=<%=month+1%>" data-role="button"  data-inline="true" data-icon="arrow-r" data-iconpos="right">다음달</a> 
			<%}%>
		</td>
	</tr>
	<tr height="10%">   
		<td align="right" colspan="3"> 
		<a href="#" onclick="ThisMonth()" data-role="button"  data-inline="true" data-icon="search" data-iconpos="right">금월조회</a>
		</td>
	</tr>   
 </table>   
<table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF" > 
<thead>
	<tr bgcolor="#CECECE" height="40" > 
	<td width='14%' bgcolor="#F29661">   
	<DIV align="center">일요일</DIV>
	</td> 
	<td width='14%' bgcolor="#BDBDBD">
	<DIV align="center">월요일</DIV>
	</td>
	<td width='14%' bgcolor="#BDBDBD"> 
	<DIV align="center">화요일</DIV>
	</td> 
	<td width='14%' bgcolor="#BDBDBD">
	<DIV align="center">수요일</DIV>
	</td>
	<td width='14%' bgcolor="#BDBDBD">  
	<DIV align="center">목요일</DIV> 
	</td>
	<td width='14%' bgcolor="#BDBDBD">
	<DIV align="center">금요일</DIV> 
	</td>
	<td width='14%' bgcolor="#86E57F"> 
	<DIV align="center">토요일</DIV>
	</td>
</tr> 
</thead>
<tbody>
<tr bordercolor="#FFFFC2">
<%

//처음 빈공란 표시
GregorianCalendar gc = new GregorianCalendar();
SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");      
Date d = gc.getTime(); 
String str = sf.format(d);    
for(int index = 1; index < start ; index++ )
{
  out.println("<td >&nbsp;</td>");
  newLine++;
}
for(int index = 1; index <= endDay; index++) 
{  
	String color = "";
	String color2 = ""; 
	String bgcolor = ""; 
	if(newLine == 0){			color = "#FFFFFF"; bgcolor = "#FFFFFF"; color2="#FF8224";  
	}else if(newLine == 6){ 	color = "#FFFFFF"; bgcolor = "#FFFFFF"; color2="#59DA50";
	}else{						color = "#FFFFFF"; bgcolor = "#FFFFFF"; color2="#818181"; }; 
	String sUseDate = Integer.toString(year);
	sUseDate += Integer.toString(month+1).length() == 1 ? "0" + Integer.toString(month+1) : Integer.toString(month+1);
	sUseDate += Integer.toString(index).length() == 1 ? "0" + Integer.toString(index) : Integer.toString(index);
	//반환된 달력일.
	int iUseDate = Integer.parseInt(sUseDate);   
	
	out.println("<td valign='top' align='left' height='100px' bgcolor='"+bgcolor+"' nowrap>");
	out.println("<table cellpading='2' height='40px' cellspacing='2' border='0' width='100%'></tr><td align='center' bgcolor='#EAEAEA'><font color='"+color2+"' size='3'><b>"+index+"</b></font></td></tr></table>");
	out.println("<table border='0'height='100px' width='100%' cellspacing='0' cellpadding='0' style='text-align:center'>"); 
	out.println("<tr><td bgcolor='#FFFFFF' nowrap id='"+iUseDate+"' onclick='fnregist("+iUseDate+")' ><img src='' bgcolor='#FFFFFF'  width='1'/></td></tr>");
	
	int count=0;

	for(int i=0;i<data.size();i++){
		String ye = data.get(i).getDATE().substring(0,4);
		String mo = data.get(i).getDATE().substring(5,7);
		String da = data.get(i).getDATE().substring(8,10);
		String dd = ye+mo+da;
		if(dd.trim().equals(sUseDate)){
			count++;
		}
	}
	if(count==0){
		out.println("<tr><td bgcolor='#FFFFFF' nowrap id='"+iUseDate+"' onclick='fnregist("+iUseDate+")' ><img src='' bgcolor='#FFFFFF'  width='1'/></td></tr>");
	}else{
		out.println("<tr><td bgcolor='#FFFFFF' nowrap id='"+iUseDate+"' onclick='fnregist("+iUseDate+")' >일정</td></tr>");
	}
		
	out.println("</table>"); 
	out.println("</td>");    
	newLine++;  
 
			if(newLine == 7)
			{
			  out.println("</tr>");
			  if(index <= endDay)
			  { 
			    out.println("<tr>");
			  }
			  newLine=0; 
			} 
}//for
 
//마지막 공란 LOOP 
while(newLine > 0 && newLine < 7)
{
  out.println("<td>&nbsp;</td>");
  newLine++;
}
 
%>



<div data-role="footer" data-position="fixed">
	<div data-role="navbar">
		<ul>
			<li><a href="#" onclick="memberManage()"   data-icon="grid">고객관리</a></li>
			<li><a href="#" onclick="estimate();" data-icon="star">견적서</a></li>
			<li><a href="#" onclick="mSMS()" data-icon="gear">SMS</a></li>
			<li><a href="#" onclick="calander()" data-icon="grid">일정</a></li>
		</ul>
	
	</div>
</div>
</div>
</body>
</html>