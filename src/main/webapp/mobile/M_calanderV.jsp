<%@page import="MVC.mobile.M_calanderBeans"%>
<%@page import="MVC.BoardBean"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<title>Insert title here</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0, minimum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script	src="../framework/jquery.mobile-1.2.0.min.js"></script>
<link rel="stylesheet"	href="../framework/jquery.mobile-1.2.0.min.css" />
<!-- <link rel="shortcut icon" href="img3/icon.png"> -->
<script src="../framework/my.js"></script>

<%
request.setCharacterEncoding("euc-kr");

	BoardBean board = (BoardBean)request.getAttribute("boardBean");
	String mobile = (String)request.getAttribute("mobile");
	
	String Date = (String)request.getAttribute("Date");
	M_calanderBeans mm = (M_calanderBeans)request.getAttribute("schedule");
	
%>
<script type="text/javascript">
function home(){
	location.href = "M_main.do?mobile=<%=mobile%>";
}

function memberManage(){
	location.href = "mMemberManage.do";
}

function estimate(){
	location.href="partList.do?mobile=<%=mobile%>";
}

function calander(){
	location.href="mCalander.do";
}

function mSMS(){
	location.href="mSms.do";
}

function upDa(){
	var f = document.form1;
	f.action2.value="update";
	f.action = "mCalanderControl.do";
	f.submit();
}

function Del(date){
	var f = document.form1;
	f.action2.value="del";
	f.action = "mCalanderControl.do";
	f.submit();
}

</script>
</head>
<body>
<form action="" name="form1" method="post">
<input type = "hidden" name="action2" value="">
<input type = "hidden" name="DATE" value="<%=mm.getDATE().substring(0,10)%>">
<div data-role="page">
	<div data-role="header"> 
		<h3> da-tec </h3>
		 <a href="#" data-rel="back" data-direction="reverse"  data-icon="back">이전</a>
		<a href="#" data-role="button" onclick="home()" data-direction="reverse"  data-icon="home">홈</a>
	</div>
	<br>
	<br>
	<table>
		<tr align="center">
			<td colspan="2">
			   <%=mm.getDATE().substring(0,10) %> 일정		
			</td>
		</tr>
		<tr>
		<td colspan="2">
			<textarea cols="40" name="CONTENTS"rows="15" id="CONTENTS"><%=mm.getCONTENTS() %></textarea>
		</td>
		</tr>	
		<tr>
			<td>
			<a data-role="button" href="#" onclick="upDa();" >수정</a>
			</td>
			<td>
			<a data-role="button" href="#" onclick="Del();" >삭제</a>
			</td>
			
		</tr>
	</table>
	
	
	<div data-role="footer" data-position="fixed">
	<div data-role="navbar">
		<ul>
			<li><a href="#" onclick="memberMange()"   data-icon="grid">고객관리</a></li>
			<li><a href="#" onclick="estimate();" data-icon="star">견적서</a></li>
			<li><a href="#" onclick="mSMS()" data-icon="gear">SMS</a></li>
			<li><a href="#" onclick="calander()" data-icon="grid">일정</a></li>
		</ul>
	
	</div>
</div>
</div>
</form>
</body>
</html>