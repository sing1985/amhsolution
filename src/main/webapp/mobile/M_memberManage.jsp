<%@page import="MVC.BoardBean"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0, minimum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script	src="../framework/jquery.mobile-1.2.0.min.js"></script>
<link rel="stylesheet"	href="../framework/jquery.mobile-1.2.0.min.css" />
<script src="../framework/my.js"></script>

<%
	BoardBean board = (BoardBean)request.getAttribute("boardBean");
	String mobile = (String)request.getAttribute("mobile");
%>

<script type="text/javascript">
function home(){
	location.href = "M_main.do?mobile=<%=mobile%>";
}

function memberManage(){
	location.href = "mMemberManage.do";
}

function estimate(){
	location.href="partList.do?mobile=<%=mobile%>";
}

function calander(){
	location.href="mCalander.do";
}

function mSMS(){
	location.href="mSms.do";
}

function Detail(val){
	location.href="M_memberDetail.do?PAGE="+val;
} 
</script>
<title>다텍 모바일 메인 페이지</title> 
</head>
<body>
<div data-role="page">
	<div data-role="header"> 
		<h3> da-tec </h3>
		 <a href="#" data-rel="back" data-direction="reverse"  data-icon="back">이전</a>
		<a href="#" data-role="button" onclick="home()" data-direction="reverse"  data-icon="home">홈</a>
	</div>
<div data-role="content">
	<ul data-role="listview">
		<li><a href="#" onclick="Detail('1')"><img src="/mobile/img/photo01.jpg" />홍길동<br>연락처 : 010-1111-1111</a></li>
		<li><a href="#" onclick="Detail('2')"><img src="/mobile/img/photo02.jpg" />김길동<br>연락처 : 010-1111-1212</a></li>
		<li><a href="#" onclick="Detail('3')"><img src="/mobile/img/photo03.jpg" />양길동<br>연락처 : 010-3333-3333</a></li> 
		<li><a href="#" onclick="Detail('4')"><img src="/mobile/img/photo04.jpg" />최길동<br>연락처 : 010-4444-4444</a></li>
	</ul>
</div>	
 
<div data-role="footer" data-position="fixed">
	<div data-role="navbar">
		<ul>
			<li><a href="#" onclick="memberMange()"   data-icon="grid">고객관리</a></li>
			<li><a href="#" onclick="estimate();" data-icon="star">견적서</a></li>
			<li><a href="#" onclick="mSMS()" data-icon="gear">SMS</a></li>
			<li><a href="#" onclick="calander()" data-icon="grid">일정</a></li>
		</ul>
	
	</div>
</div>
</div>
</body>
</html>