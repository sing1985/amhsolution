<%@page import="MVC.BoardBean"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0, minimum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script	src="../framework/jquery.mobile-1.2.0.min.js"></script>
<link rel="stylesheet"	href="../framework/jquery.mobile-1.2.0.min.css" />
<script src="../framework/my.js"></script>

<%
String PAGE = (String) request.getAttribute("PAGE");
System.out.println("출력되는 페이지 : " + PAGE);
 
	BoardBean board = (BoardBean)request.getAttribute("boardBean");
	String mobile = (String)request.getAttribute("mobile");
%>

<script type="text/javascript">
function home(){
	location.href = "M_main.do?mobile=<%=mobile%>";
}

function memberManage(){
	location.href = "mMemberManage.do";
}

function estimate(){
	location.href="partList.do?mobile=<%=mobile%>";
}

function calander(){
	location.href="mCalander.do";
}

function mSMS(){
	location.href="mSms.do";
}

function Detail(val){
	location.href="mMemberMDetail.do?PAGE="+val;
}

</script>
</head>
<body>
<div data-role="page">
	<div data-role="header"> 
		<h3>고객정보</h3>
		 <a href="#" data-rel="back" data-direction="reverse"  data-icon="back">이전</a>
		<a href="#" data-role="button" onclick="home()" data-direction="reverse"  data-icon="home">홈</a>
	</div>
<div data-role="content"> 
<%
if(PAGE.equals("1")){
%><center>
	<img src = "/mobile/img/photo01.jpg" width="80%"/>
</center>
<br>
고객성명 : 홍길동<br>
생년월일 : 1990.05.15<br>
연 락 처 : 010-1111-1111<br>
주    소 : 광주광역시 서구 화정동<br>
E-mail: abcd@efgh.com<br>
<%
}else if(PAGE.equals("2")){
	%><center>
	<img src = "/mobile/img/photo02.jpg" width="80%"/>
</center>
<br>
고객성명 : 김길동<br>
생년월일 : 1991.07.14<br>
연 락 처 : 010-1111-1212<br>
주    소 : 조선대학교<br>
E-mail: abcd@efgh.com<br>
<%
}else if(PAGE.equals("3")){
	%><center>
	<img src = "/mobile/img/photo03.jpg" width="80%"/>
</center>
<br>
고객성명 : 양길동<br>
생년월일 : 1991.07.14<br>
연 락 처 : 010-3333-3333<br>
주    소 : 조선대학교<br>
E-mail: abcd@efgh.com<br>
<%
}else if(PAGE.equals("4")){
	%><center>
	<img src = "/mobile/img/photo04.jpg" width="80%"/>
</center>
<br>
고객성명 : 최길동<br>
생년월일 : 1991.07.14<br>
연 락 처 : 010-4444-4444<br>
주    소 : 조선대학교<br>
E-mail: abcd@efgh.com<br>
<%
}
%>
</div>	
 

<div data-role="footer" data-position="fixed">
	<div data-role="navbar">
		<ul>
			<li><a href="#" onclick="memberManage()"   data-icon="grid">고객관리</a></li>
			<li><a href="#" onclick="estimate();" data-icon="star">견적서</a></li>
			<li><a href="#" onclick="mSMS()" data-icon="gear">SMS</a></li>
			<li><a href="#" onclick="calander()" data-icon="grid">일정</a></li>
		</ul>
	 
	</div>
</div>
</div>
</body>
</html>